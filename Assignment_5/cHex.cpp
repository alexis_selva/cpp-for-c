// Module for representing the hex game

// Dependancies
#include "cHex.hpp"

// Constructor: generate the hex board game
cHex::cHex(size_t dimension, ePlayerMode playerMode1, ePlayerMode playerMode2) : moves(0), dimension(dimension) {
	
	// Initialize the players
	players[0].setMode(playerMode1);
	players[1].setColor(ePlayerColor::RED);
	players[1].setMode(playerMode2);
	
	// Initialize the edges between the vertices
	initialize();
}

// Contructor: create a hex game from an existing one
cHex::cHex(const cHex& hex) : cGraph(hex), moves(hex.moves), dimension(hex.dimension) {
	
	// Initialize the players
	players[0].setMode(hex.players[0].getMode());
	players[1].setColor(ePlayerColor::RED);
	players[1].setMode(hex.players[1].getMode());
}

// Destructor: destroy the hex board game
cHex::~cHex(void){
}

// Accessor: return the dimension of the game
size_t cHex::getDimension(void){
	return dimension;
}

// Accessor: return the number of moves 
size_t cHex::getMoves(void){
	return moves;
}

// Function nitializing the virtual vertex by adding edges between it and the nodes of the corresponding border
void cHex::initializeVirtualVertex(size_t begin, size_t sentinel, size_t increment, eVertexColor color, size_t virtualVertex){
	for (size_t source = begin; source < sentinel; source += increment){
		add(source, virtualVertex);
	}
	setNodeColor(virtualVertex, color);
}

// Function initializing the game
void cHex::initialize(void){
	
	// Add the vertices into the graph
	add(dimension * dimension + NB_VIRTUAL_VERTICES);
	
	// Get the number of non virtual vertices
  size_t nbVertices = V() - NB_VIRTUAL_VERTICES;
  
  // Create the edges between the vertices
	for (size_t vertex = 0 ; vertex < nbVertices ; vertex++){
		
		// Determine the location (upper / lower / intern) of the vertex
		if (isUpBound(vertex)){
			
			// Determine the location (left / right / intern) of the vertex
			if (isLeftBound(vertex)){
				add(vertex, vertex + 1);
				setEdgeValue(vertex, vertex + 1, 1);
				add(vertex, vertex + dimension);	
				setEdgeValue(vertex, vertex + dimension, 1);
			} else if (isRightBound(vertex)){
				add(vertex, vertex + dimension - 1);
				setEdgeValue(vertex, vertex + dimension - 1, 1);
				add(vertex, vertex + dimension);
				setEdgeValue(vertex, vertex + dimension, 1);
			} else {
				add(vertex, vertex + 1);
				setEdgeValue(vertex, vertex + 1, 1);
				add(vertex, vertex + dimension - 1);
				setEdgeValue(vertex, vertex + dimension - 1, 1);
				add(vertex, vertex + dimension);
				setEdgeValue(vertex, vertex + dimension, 1);
			}
			
		} else if (isLowBound(vertex)){
			
			// Determine the location (left / right / intern) of the vertex
			if (isLeftBound(vertex)){
				add(vertex, vertex + 1);
				setEdgeValue(vertex, vertex + 1, 1);
			} else if (isRightBound(vertex)){
				; //nop
			} else {
				add(vertex, vertex + 1);
				setEdgeValue(vertex, vertex + 1, 1);
			}
			
		} else {
			
			// Determine the location (left / right / intern) of the vertex
			if (isLeftBound(vertex)){
				add(vertex, vertex + 1);
				setEdgeValue(vertex, vertex + 1, 1);
				add(vertex, vertex + dimension);	
				setEdgeValue(vertex, vertex + dimension, 1);
			} else if (isRightBound(vertex)){
				add(vertex, vertex + dimension - 1);
				setEdgeValue(vertex, vertex + dimension - 1, 1);
				add(vertex, vertex + dimension);
				setEdgeValue(vertex, vertex + dimension, 1);
			} else {
				add(vertex, vertex + 1);
				setEdgeValue(vertex, vertex + 1, 1);
				add(vertex, vertex + dimension - 1);
				setEdgeValue(vertex, vertex + dimension - 1, 1);
				add(vertex, vertex + dimension);
				setEdgeValue(vertex, vertex + dimension, 1);
			}
		}
	}
	
	// Initialize the virtual vertices
	initializeVirtualVertex(0, dimension * dimension - dimension + 1, dimension, eVertexColor::BLUE, getEastVirtualVertex());
	initializeVirtualVertex(dimension - 1, dimension * dimension, dimension, eVertexColor::BLUE, getWestVirtualVertex());
	initializeVirtualVertex(0, dimension, 1, eVertexColor::RED, getNorthVirtualVertex());
	initializeVirtualVertex(dimension * dimension - dimension, dimension * dimension, 1, eVertexColor::RED, getSouthVirtualVertex());
}

// Function determining the next human location
size_t cHex::determineHumanLocation(void){

	// Declare the vertex id
	size_t vertex(0);

	// Declare a loop variable
	bool legal(true);
	
	// Display the menu and return a correct answer
	do {
				
		// Display a message
		cout << "Enter location: ";
		
		// Declare the location
		string answer;
		
		// Read the answer from stdin
		cin >> answer;
		
		// Extract the different parts of the answer
		size_t part1 = static_cast<size_t>(answer.at(0) - 'A');
		size_t part2 = 0;
		try {
			part2 = stoi(answer.substr(1)) - 1;
			legal = part1 >= 0 && part1 < dimension && part2 >= 0 && part2 < dimension;
		} catch (const invalid_argument& arg){
			legal = false;
		}
		
		// Check the different parts of the answer
		if (legal){
			
			// Determine the corresponding vertex
			vertex = part2 * dimension + part1;

			// Update the retry variable
			legal &= !isOutOfBound(vertex);
			
			// Check the vertex is not out of bound
			if (legal){
				
				// Check the location is free
				legal = (getNodeColor(vertex) == eVertexColor::WHITE);
				if (!legal){
					cout << "ERROR - Location already occupied !" << endl;
				}
				
			} else {
				cout << "ERROR - Invalid position !" << endl;
			}
			
		} else {
			cout << "ERROR - Invalid position !" << endl;
		}
		
	} while (!legal);

	return vertex;
}
	
// Function determining the AI location
size_t cHex::determineAiLocation(size_t currentIdPlayer){
	
	// Get the time at the beginning of the function
  time_t start;
  time(&start);
	
	// Declare legal moves vector
	vector <size_t> legalMoves;
	
	// Determine all legal available moves
  for (size_t vertex(0); vertex < dimension * dimension; vertex++) {
    if (isLegal(vertex)){
			legalMoves.push_back(vertex);
		}
	}
		
	// Declare the bestMove
	size_t bestMove(legalMoves.front());
	
	// Declare the maximum nuber of wins
	size_t maxNbWins (0);
	
	// Copy the hex game
	cHex copyHex(*this);
	
	// Evaluate all moves
	for (auto firstMove : legalMoves){
		
		// Declare the number of wins
		size_t nbWins(0);
		
		// Set the color of the first move
		copyHex.setNodeColor(firstMove, copyHex.players[currentIdPlayer].getColor());
		
		// Perform the trials
		for (size_t trial = 0 ; trial < NB_TRIALS ; trial++){
			
			// Store the current id of the player
			size_t idPlayer = currentIdPlayer;
			
			// Declare the remaining moves vector from the legal move
			vector <size_t> remainingMoves(legalMoves.size() - 1);
			
			// Remove the move being evaluated
			std::remove_copy(legalMoves.begin(), legalMoves.end(), remainingMoves.begin(), firstMove);
			
			// Reorder the moves randomly 
			unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
			shuffle (remainingMoves.begin(), remainingMoves.end(), default_random_engine(seed));
			
			// Fill the hex board up
			copyHex.fillUp(remainingMoves, idPlayer);
						
			// Check if the AI player wins at the end
			if (copyHex.win(firstMove)){
				
				// Increment the number of wins
				nbWins++;
			} 
						
			// Rollback the hex board for the next trial (avoid to call again the Copy constructor)
			copyHex.rollback(remainingMoves, idPlayer);
		}
		
		// Reset the color of the first move
		copyHex.setNodeColor(firstMove, eVertexColor::WHITE);
		
		// Evaluate the best move by considering the ratio: wins/trials
		if (nbWins > maxNbWins){
			
			// Store the maximum number of wins
			maxNbWins = nbWins;
			
			// Store the best move
			bestMove = firstMove;
		}		
	}
	
	// Get the time at the end of the function
  time_t end;
  time(&end);
	
	// Display a message
	cout << "Enter location: " << toString(bestMove) << " (after " << end - start << " s)" << endl;
	
	// Return the best move
	return bestMove;
}

// Function allowing to set the color to the next location
size_t cHex::next(size_t idPlayer){
			
	// Print a description
	cout << "Move " << moves + 1 << ": " << players[idPlayer] << endl;

	// Declare the next vertex to color
	size_t vertex (0);

	// Determine if the player is human
	if (players[idPlayer].isHuman()){
		
		// Determine the vertex to color
		vertex = determineHumanLocation();
		
	} else {
		
		// Determine the vertex to color
		vertex = determineAiLocation(idPlayer);
	}
			
	// Set the color of the vertex
	setNodeColor(vertex, players[idPlayer].getColor());
	cout << "--------------------------------------------------------------------------------" << endl;
	
	// Return the vertex
	return vertex;
}

// Function determining if a move wins the game
bool cHex::win(size_t move){
	
	// Initialize MST with the board
	cMst mst (this);
	
	// Get the move color
	eVertexColor color = getNodeColor(move);
	
	// Declare the virtual vertices to reach
	size_t virtualVertex1 (0);
	size_t virtualVertex2 (0);
	
	// Apply Prim's algorithm from the move
	mst.applyPrim(move);
	
	// Determine the color of the player
	if (color == eVertexColor::BLUE) {

		// Set the virtual vertex to reach
		virtualVertex1 = getEastVirtualVertex();
		virtualVertex2 = getWestVirtualVertex();
		
	} else {
		
		// Set the virtual vertex to reach
		virtualVertex1 = getNorthVirtualVertex();
		virtualVertex2 = getSouthVirtualVertex();
	}
	
	// Determine the MST
	tTree tree = mst.getTree();
				
	// Get all edges of the MST
	bool foundvirtualVertex1 = false;
	bool foundvirtualVertex2 = false;	
	for (auto pair : tree){

		// Get the edge
		cEdge * edge = pair.second;
						
		// Determine if the remote vertex of the edge is one of the expected virtual vertices
		if (virtualVertex1 == edge->getVertex()) {
			foundvirtualVertex1 = true;
		} else if (virtualVertex2 == edge->getVertex()) {
			foundvirtualVertex2 = true;
		}
		
		// Determine if both expected vertices belong to the MST 
		if ( (foundvirtualVertex1) && (foundvirtualVertex2) ){
			return true;
		}
	}

	return false;
}

// Function managing the game
void cHex::play(void){
		
	// Display the rules
	printRules();
	
	// Display the board
	printBoard();
	
	// Declare the player index
	size_t idPlayer (0);
	
	// Declare a loop variable
	bool loop(true);
	
	// Repeat until the game is finished
	do {
		
		// Set the next color
		size_t vertex = next(idPlayer);
		
		// Increment the number of moves
		moves++;
		
		// Display the board
		printBoard();
		
		// Update the loop variable
		loop = !win(vertex);
	
		// Check that a player wins
		if (loop){
			
			// Determine the next player id
			idPlayer = (idPlayer + 1) % NB_PLAYERS;
			
		} else {
			
			// Display the winner
			cout << players[idPlayer] << " wins !" << endl;
			cout << "--------------------------------------------------------------------------------" << endl;
		}
		
	} while (loop);
}

// Function filling the board up
void cHex::fillUp(const vector <size_t> remainingMoves, size_t& idPlayer){
	
	// Apply the player colors for each remaining moves
	for (auto move : remainingMoves) {
								
		// Determine the next player id
		idPlayer = (idPlayer + 1) % NB_PLAYERS; 
				
		// Set the color of the move (vertex)
		setNodeColor(move, players[idPlayer].getColor());	
	}
}

// Function rollbacking the board
void cHex::rollback(vector <size_t> remainingMoves, size_t& idPlayer){
	
	// Reverse the order of the remaining moves
	reverse(remainingMoves.begin(),remainingMoves.end());
	
	// Apply the player colors for each remaining moves
	for (auto move : remainingMoves) {
				
		// Reset the color of the move (vertex)
		setNodeColor(move, eVertexColor::WHITE);	
		
		// Determine the next player id
		idPlayer = (idPlayer + 1) % NB_PLAYERS; 
	}
}
	
// Function printing the rules
void cHex::printRules(void){
	cout << "RULES:" << endl;
	cout << " * Blue Human location 	(X) => objective: start the game and connect WEST-EAST" << endl;
	cout << " *  Red Human location 	(0) => objective: connect NORTH-SOUTH" << endl;
	cout << " *      Empty location 	(.)" << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
}

// Function printing the board recursively
void cHex::printBoard(void){
	
	// Print description
	if (moves > 0){
		cout << "BOARD (after move " << moves << ")" << endl;
	} else {
		cout << "BOARD (no moves)" << endl;
	}

	// Print the NORTH border
	cout << endl << "   ";
	for (char legend = 'A' ; legend < 'A' + dimension ; legend++){
		cout << legend << "   ";
	}
	
	// Declare the lines to print
	string currentLine;
	string nextLine;
	
	// Declare the legend to print
	size_t legend = 0;
		
	// Get all non-virtual vertices
	for (size_t vertexId = 0 ; vertexId < V() - NB_VIRTUAL_VERTICES ; vertexId++) {
		
		// Update the lines if necessary
		if (isLeftBound(vertexId)){
			legend++;
			if (legend < 10){
				currentLine.append((vertexId / dimension + 1) * 2 , ' ');
			} else {
				currentLine.append((vertexId / dimension + 1) * 2 - 1 , ' ');
			}
			stringstream stream;
			stream << legend;
			currentLine.append(stream.str());
			currentLine.append(" ");
			nextLine.append((vertexId / dimension + 2) * 2 + 1, ' ');
		}
		
		// Get the vertex
		cVertex* vertex = getVertices().at(vertexId);
		
		// Print the vertex color
		if (vertex->getColor() == eVertexColor::BLUE){
			currentLine.append("X");
		} else if (vertex->getColor() == eVertexColor::RED){
			currentLine.append("0");
		} else {
			currentLine.append(".");
		}
		
		// Get the edges of the vertex
		for (auto edge : vertex->getEdges()){
			
			// Determine if it is the next vertex
			if ( (edge->getVertex() > vertexId) && (!isVirtual(edge->getVertex())) ) {
				
				// Determine if it is the current line or the next one
				if (edge->getVertex() == vertexId + 1){
					currentLine.append(" _ ");
				} else if (edge->getVertex() == vertexId + dimension - 1){
					nextLine.append("/ ");
				} else if (edge->getVertex() == vertexId + dimension){
					nextLine.append("\\ ");
				}
			}
		}
		
		// Determine the vertex is located on the right bound
		if (isRightBound(vertexId)){
			
			// Print the legend
			currentLine.append(" ");
			stringstream stream;
			stream << legend;
			currentLine.append(stream.str());
					
			// Print the current line and the next one
			cout << endl << currentLine << endl << nextLine;
			
			// Clear the current line 
			currentLine.clear();
			
			// Clear the next line
			nextLine.clear();
		} 
	}
	
	// Print the SOUTH border
	cout << endl;
	currentLine.clear();
	currentLine.append((dimension + 1) * 2 + 1, ' ');
	cout << currentLine;
	for (char legend = 'A' ; legend < 'A' + dimension ; legend++){
		cout << legend << "   ";
	}
	cout << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
}
