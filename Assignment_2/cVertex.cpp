// Module for representing a vertex

#ifndef CVERTEX_CPP
#define CVERTEX_CPP

// Dependancies
#include <iostream>
#include <list>
#include "cEdge.cpp"

// Namespace
using namespace std;

// Constante in case of unknown vertex
const double UNKNOWN = 0;

// Class: cVertex
class cVertex {
  
  private:
    size_t id;
    double value;
    list <cEdge*> edges;
  
  public:
    cVertex(size_t vertexId);
    ~cVertex(void);
    size_t getId(void);
    double getValue(void);
    list <cEdge*>& getEdges();
    void setId(size_t vertexId);
    void setValue(double newValue);
    void setEdges(list <cEdge*> newEdges);
    void add(size_t vertexId); 
    bool isPresent(size_t vertexId);  
    double getCost(size_t vertexId);
    void setCost(size_t vertexId, double newCost);
    void remove(size_t vertexId);
    void clear(void); 
};

// Constructor: : create a new vertex
cVertex::cVertex(size_t vertexId){
  id = vertexId;
  value = 0;
}

// Destructor: : destroy the vertex
cVertex::~cVertex(void){
}

// Accessor: get the id of the vertex
inline size_t cVertex::getId(void){
  return id;
}

// Accessor: get the value of the vertex
inline double cVertex::getValue(void){
  return value;
}

// Accessor: get the edges (reference)
inline list <cEdge*>& cVertex::getEdges(void){
  return edges;
}

// Mutator: set the id of the vertex
inline void cVertex::setId(size_t newVertexId){
  id = newVertexId;
}

// Mutator: set the value of the vertex
inline void cVertex::setValue(double newValue){
  value = newValue;
}

// Mutator: set the edges
inline void cVertex::setEdges(list <cEdge*> newEdges){
  edges = newEdges;
}

// Function adding a edge between 2 vertices
inline void cVertex::add(size_t vertexId){
  if (!isPresent(vertexId)){
    edges.push_back(new cEdge(vertexId));
  }
}

// Function checking if an edge exists between 2 vertices
inline bool cVertex::isPresent(size_t vertexId){
  cEdge* edge = NULL;  
  bool found = false;
  list<cEdge*>::iterator it = edges.begin();
  bool loop = it!=edges.end();
  while (loop){
      
    // Get the edge (reference)   
    edge = *it;
      
    // Is it the one we are looking for ?  
    found = (*edge == vertexId);
    
    // Get the next ?
    ++it;
    
    // Shall we continue ?
    loop = (it!=edges.end()) and (!found);
  }
  
  return found;
}

// Function getting the cost of a edge corresponding to a particular remote vertex
inline double cVertex::getCost(size_t vertexId){
  cEdge* edge = NULL;   
  double cost = 0;
  bool found = false;
  list<cEdge*>::iterator it = edges.begin();
  bool loop = it!=edges.end();
  while (loop){
      
    // Get the edge (reference)   
    edge = *it;
    
    // Is it the one we are looking for ?
    found = (*edge == vertexId);
    
    // Get the next
    ++it;
    
    // Shall we continue ?
    loop = (it!=edges.end()) and (!found);
  }
  
  // Get the cost
  if (found){
    cost = edge->getCost();
  } 
  
  return cost;
}

// Function setting the cost of an edge corresponding to a particular remote vertex
inline void cVertex::setCost(size_t vertexId, double newCost){
  cEdge* edge = NULL; 
  bool found = false;
  list<cEdge*>::iterator it = edges.begin();
  bool loop = it!=edges.end();
  while (loop){
      
    // Get the edge (reference) 
    edge = *it;
    
    // Is it the one we are looking for ?
    found = (*edge == vertexId);
    
    // Get the next
    ++it;
    
    // Shall we continue ?
    loop = (it!=edges.end()) and (!found);
  }
  
  // Set the cost
  if (found){
    edge->setCost(newCost);
  }
}

// Function removing an existing edge between 2 vertices
inline void cVertex::remove(size_t vertexId){
  cEdge* edge = NULL; 
  bool found = false;
  list<cEdge*>::iterator it = edges.begin();
  bool loop = it!=edges.end();
  while (loop){
      
    // Get the edge (reference) 
    edge = *it;
    
    // Is it the one we are looking for ?
    found = (*edge == vertexId);
    
    // Get the next
    ++it;
    
    // Shall we continue ?
    loop = (it!=edges.end()) and (!found);
  }
  
  // Remove the edge
  if (found){
    edges.remove(edge);
    delete(edge);
  }
}

// Function clearing any existing edges between 2 vertices
inline void cVertex::clear(void){
  if (!edges.empty()){
    edges.clear();  
  }
}

// Function overloading the operator ==
inline bool operator== (cVertex& vertex1, cVertex& vertex2){
  return (vertex1.getId() == vertex2.getId());
}

// Function overloading the operator ==
inline bool operator== (cVertex& vertex, size_t vertexId){
  return (vertex.getId() == vertexId);
}
 
// Function overloading the operator !=
inline bool operator!= (cVertex& vertex1, cVertex& vertex2){
  return !(vertex1 == vertex2);
}
 
// Function overloading the operator !=
inline bool operator!= (cVertex& vertex, size_t vertexId){
  return !(vertex == vertexId);
}

// Function overloading the operator <<
inline ostream& operator<<(ostream& out, cVertex& vertex){
  if (!vertex.getEdges().empty()){
    for (list<cEdge*>::iterator it = vertex.getEdges().begin(); it != vertex.getEdges().end(); ++it){
      cEdge* edge = *it;
      out << vertex.getId() << *edge;
    }
  } else {
      out << vertex.getId() << endl;
  }
  return out;
}

#endif
