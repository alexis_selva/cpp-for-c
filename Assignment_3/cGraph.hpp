// Module for representing a graph

#ifndef CGRAPH_HPP
#define CGRAPH_HPP

// Dependancies
#include <iostream>
#include <vector>
#include <stdlib.h> 
#include <time.h>
#include <cmath>
#include <fstream>
#include "cVertex.hpp"

// Namespace
using namespace std;

// Constant representing the default number of vertices
const size_t DEFAULT_NUMBER_VERTICES = 50;

// Class: cGraph
class cGraph {
  
  private:
    vector<cVertex*> vertices;
    size_t nbEdges;
  
  public:
    cGraph(void);
    cGraph(char * inputFile);
    ~cGraph(void);
    vector<cVertex*>& getVertices(void);
    size_t getNbEdges(void);
    void setVertices(vector<cVertex*> vertices);
    void setNbEdges(size_t newNbEdges);
    size_t V(void);
    size_t E(void);
    bool adjacent(size_t vertexX, size_t vertexY);
    list<cEdge*> neighbors(size_t vertexX);
    void add(size_t nb);
    void add(size_t vertexX, size_t vertexY);
    bool isPresent(size_t vertex);
    void remove(size_t vertexX, size_t vertexY);
    eVertexColor getNodeColor(size_t vertex);
    bool setNodeColor(size_t vertex, eVertexColor color);
    double getNodeValue(size_t vertex);
    void setNodeValue(size_t vertex, double a);
    double getEdgeValue(size_t vertexX, size_t vertexY);
    void setEdgeValue(size_t vertexX, size_t vertexY, double cost);
		void generateRandomEdges(double edgeDensity, double distanceMin, double distanceMax);
    friend ostream& operator<<(ostream& out, cGraph& graph);
    
	private:
		void getNbVertices(size_t& nbVertices, ifstream& inputFile);
		void getEdge(size_t& vertexX, size_t& vertexY, double& distance, ifstream& inputFile);
};

#endif
