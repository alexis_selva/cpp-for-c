// Objective: implementation of a minimum spanning tree (MST) algorithm for a weighted undirected graph 

// Sources:
//  * http://en.wikipedia.org/wiki/Prim_algorithm
//  * http://www.cplusplus.com

// Explanation:
//  Given that the graphs generated during these simulations are sparse, I decided to represent the graph as 
//  an adjacency list. More concretly, I defined the graph (cf. cGraph module) as a vector of vertex pointers. 
//  Each vertex (cf. cVertex.cpp module) is represented by an id, a value (priority in the Dijkstra context) and a 
//  list of edge pointers. Each edge (cf. cEdge module) is defined by its cost and a remote vertex id.
//  To compute the minimum spanning tree, I implemented Prim's algorithm (cf. cMst module).

// Output considering the Coursera input data file:
//  MST (cost = 30):
//  0		----	2	----	2
//  2		----	1	----	9
//  9		----	3	----	8
//  8		----	1	----	4
//  4		----	1	----	7
//  4		----	2	----	15
//  7		----	2	----	10
//  15	----	2	----	19
//  10	----	3	----	16
//  16	----	2	----	5
//  5		----	1	----	6
//  5		----	1	----	18
//  6		----	1	----	1
//  18	----	1	----	14
//  1		----	1	----	17
//  14	----	1	----	11
//  17	----	1	----	12
//  12	----	1	----	3
//  9		----	3	----	13

// Dependancies
#include <iostream>
#include "cMst.hpp"

// Namespace
using namespace std;

// Main function
int main(int argc, char* argv[]){
	
  // Check the number of arguments
  if (argc >= 2){
	    	 
	// Generate a graph from the input filename and determine one of its MST
	cGraph graph(argv[1]);
	cMst mst(&graph);
	mst.applyPrim(0);
	  
	// Dump the determined MST
	cout << mst;
	 
  } else {
	  
	// Generate a random graph and determine one of its MST
	cMst mst;
	  
	// Dump the determined MST
	cout << mst;
  }

  // Return OK
  return 0;
}

