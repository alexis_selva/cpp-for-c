// Convert this program to C++
//   change to C++ io
//   change to one line comments
//   change defines of constants to const
//   change array to vector<>
//   inline any short function

// External source: http://www.cplusplus.com/reference/vector/vector

// Dependancies
#include <iostream>
#include <vector>

// Namespace
using namespace std;

// Constant representing the max size of the vector
const unsigned VECTOR_MAX_SIZE = 40;

// Function to sum the elements of the vector
inline void sum(int &res, int size, vector<int> myVector){
	res = 0;
	for(unsigned i = 0 ; i < static_cast<unsigned>(size) ; ++i){
		res += myVector.at(i);
	}
}

// Main function
int main(){

	// Declare a vector
	vector<int> data(VECTOR_MAX_SIZE);
	
	// Fill the vector
	for(unsigned i = 0 ; i < VECTOR_MAX_SIZE ; ++i) {
		data.at(i) = i;
	}
    
    // Declare a variable to sum the elements of the vector
    int sumData = 0;
    
    // Sum the elements of the vector
	sum(sumData, VECTOR_MAX_SIZE, data);
	
	// Display the sum
	cout << "sum is " << sumData << endl;
	
	// Return OK
	return 0;
}
