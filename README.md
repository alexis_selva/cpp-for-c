These are the programming assignments relative to C++ for C programmers:

* Assignment 1: converting a simple C program to C++ one
* Assignment 2: implementing a Monte Carlo simulation that calculates the average shortest path in a graph
* Assignment 3: implementing of a minimum spanning tree (MST) algorithm for a weighted undirected graph
* Assignment 4:  making a playable version of the game of HEX (drawing a HEX board and determining a legal position and a winning position)
* Assignment 5: making a playable version of the game of HEX (adding an AI that can play HEX well)

For more information, I invite you to have a look at https://www.coursera.org/course/cplusplus4c