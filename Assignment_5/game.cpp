// Objective: make a playable version of the game of HEX

// Source:
//  * http://www.cplusplus.com

// Explanation:
//  I decided to represent the Hex game by a class (cf. cHex module) inherited from my graph class (cf. cGraph module). The graph is a vector 
//  of vertex pointers. Each vertex (cf. cVertex module) is represented by an id, a color, a value (priority in the Dijkstra context) 
//  and a list of edge pointers. Each edge (cf. cEdge module) is defined by its cost and a remote vertex id. Then, I created 4 virtual 
//  vertexes which represent the different borders (NORTH, SOUTH, WEST and EAST). Finally to identify if a player (BLUE or RED) wins 
//  the party, I reused Prim's algorithm from the best selected move in order to evaluate if the targeted virtual vertices
//  belong to the generated MST.

// Dependancies
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include "cHex.hpp"

// Namespace
using namespace std;

// Constant representing the number of hexes possible
const size_t MIN_HEX_BOARD = 3;
const size_t MAX_HEX_BOARD = 26;

// Function determining the number of hexes
size_t determineNumberHexes(void){
	
	// Read the number of hexes
	size_t numberHexes (0);
	bool retry (false);
	do {
		
		// Request the number of hexes
		cout << "Enter the number of hexes per side: ";
		string answer;
		cin >> answer;
		try {
			numberHexes = stoi(answer);
			retry = (numberHexes < MIN_HEX_BOARD) || (numberHexes > MAX_HEX_BOARD);
			if (retry) {
				cout << "ERROR - Invalid number of hexes !" << endl;
			}
		} catch (const invalid_argument& arg){
			cout << "ERROR - Invalid number of hexes !" << endl;
			retry = true;
		}
		
	} while (retry);
	
	// Return the number of hexes
	return numberHexes;
}

// Function determining the mode of the player
ePlayerMode determinePlayerMode(size_t player){
	
	// Declare the mode and the color of the player
	ePlayerMode mode (ePlayerMode::AI);
	ePlayerColor color (ePlayerColor::BLUE);
	
	// Change the color of the 2nd player
	if (player == 2){
		color = ePlayerColor::RED;
	}
	
	// Read the mode of the player
	bool retry (false);
	do {
		
		// Request the mode of the player
		cout << "Is the " << toString(color) << " player human ? ";
		string answer;
		cin >> answer;
		
		// Transform the case of the answer
		transform(answer.begin(), answer.end(), answer.begin(), ::toupper);
		
		if( (answer.compare("N") == 0) || (answer.compare("NO") == 0) ) {
			mode = ePlayerMode::AI;
			retry = false;
		} else if( (answer.compare("Y") == 0) || (answer.compare("YES") == 0) ) {
			mode = ePlayerMode::HUMAN;
			retry = false;
		} else {
			cout << "ERROR - Invalid answer !" << endl;
			retry = true;
		}
				
	} while (retry);
	
	// Return the mode of the player
	return mode;
	
}

// Main function
int main(void){
	
	// Print a description
	cout << "Welcome to my version of the Hex game." << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	
	// Determine the board dimension
	size_t dimension = determineNumberHexes();
	cout << "--------------------------------------------------------------------------------" << endl;
	
	// Determine the mode of the players
	ePlayerMode playerMode1 = determinePlayerMode(1);
	cout << "--------------------------------------------------------------------------------" << endl;
	ePlayerMode playerMode2 = determinePlayerMode(2);
	cout << "--------------------------------------------------------------------------------" << endl;
	
	// Construct the game
	cHex game(dimension, playerMode1, playerMode2);
	
	// Start the game
	game.play();
	
  // Return OK
  return 0;
}

