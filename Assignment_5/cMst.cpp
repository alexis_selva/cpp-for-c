// Module for computing the Minimum Spanning Tree

// Dependancy
#include "cMst.hpp"

// Contructor: create a Minimum Spanning Tree of a random graph
cMst::cMst(void) : graph(nullptr) {
	
	// Specify the comparison function to the queue constructor
	queue = tMinHeap();
}

// Contructor: create a Minimum Spanning Tree of an input graph
cMst::cMst(cGraph* inputGraph) : graph(inputGraph) {
	
	// Specify the comparison function to the queue constructor
	queue = tMinHeap();
}

// Destructor: destroy the Minimum Spanning Tree
cMst::~cMst(void){
			
	// Clear the queue
	while (!queue.empty()){
		queue.pop();
	}
		
	// Clear the tree
	tree.clear();
}

// Accessor: get the graph pointer
cGraph* cMst::getGraph(void) const {
	return graph;
}

// Accessor: get the tree
tTree& cMst::getTree(void) {
	return tree;
}

// Mutator: set the graph pointer
void cMst::setGraph(cGraph* graph){
	this-> graph = graph;
}

// Apply Prim algorithm
void cMst::applyPrim(size_t sourceId){
	
	// Verify the graph exists
	if (graph){
	
		// Get the number of vertices
		size_t nbVertices = graph->V();

		// Check the source is not out of limit
		if (sourceId < nbVertices) {
				
			// Declare an array of vertices in order to detect the loop
			vector<bool> loop (nbVertices, false);
		
			// Define the current vertex 
			cVertex * currentVertex = graph->getVertices().at(sourceId);
			
			// Mark the current vertex
			loop.at(currentVertex->getId()) = true;
		
			// Continue while the tree contains all vertices
			while (tree.size() < (nbVertices - 1)) {
		
				// Add the edges of the source to the queue
				for (auto it = currentVertex->getEdges().begin(); it != currentVertex->getEdges().end() ; it++){
			
					// Get a edge
					cEdge * edge = *it;
					
					// Get the remote vertex
					cVertex* remoteVertex = graph->getVertices().at(edge->getVertex());
					
					// Check the remote vertex has the same color as the current vertex
					if (currentVertex->getColor() == remoteVertex->getColor()){
					
						// Declare a pair representing the vertex and the edge
						tPairVertexEdge pair (currentVertex, edge);
					
						// Insert this tuple into the queue
						queue.push(pair);
					}
				}

				// Extract the pair until the minimum weight edge is found
				tPairVertexEdge pair;
				do {
					
					// Verify the queue is not empty
					if (!queue.empty()) {
						
						// Extract the min from the heap
						pair = queue.top();
						queue.pop();

					} else {
						
						// Stop the function because the Prism's algorithm cannot be applied (graph is not connected);
						return;
					}
					
				} while (loop.at(pair.second->getVertex()));
															
				// Add this edge into the tree
				tree.push_back(pair);
					
				// Update the current vertex
				currentVertex = graph->getVertices().at(pair.second->getVertex());
					
				// Mark the current vertex
				loop.at(currentVertex->getId()) = true;
			}
		}
	}
}

// Function returning the cost of the MST
double cMst::getCost(void){
	
	// Get the cost of all branchs of the tree by using lambda function
	return accumulate(tree.begin(), tree.end(), 0, [] (double value, tPairVertexEdge & pair) -> double { return value + pair.second->getCost(); } );
}

// Function overloading the operator <<
ostream& operator<<(ostream& out, cMst& mst){
	
	// Print all branchs of the tree
	out << "MST (cost = " << mst.getCost() << "):" <<  endl;
	for (auto it = mst.tree.begin() ; it != mst.tree.end() ; it++){
		
		// Print the pair (vertex, edge)
		cVertex * vertex = it->first;
		cEdge * edge = it->second;
		out << vertex->getId() << *edge;
	}
	out << endl;
	
	return out;
}
