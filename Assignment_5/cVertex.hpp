// Module for representing a vertex

#ifndef CVERTEX_HPP
#define CVERTEX_HPP

// Dependancies
#include <iostream>
#include <list>
#include "cEdge.hpp"

// Namespace
using namespace std;

// Constant in case of unknown vertex
const double UNKNOWN = 0;

// List different colors of a vertex
enum class eVertexColor : char { WHITE, BLUE, RED };

// Function converting a color to string
inline string toString(eVertexColor color){
  switch (color) {
		case eVertexColor::BLUE: 
			return "blue";
		case eVertexColor::RED: 
			return "red";
		default:
			return "white";
	}	
}

// Class: cVertex
class cVertex {
  
  private:
    size_t id;
		eVertexColor color;
    double value;
    list <cEdge*> edges;
  
  public:
    cVertex(size_t vertexId);
    cVertex(const cVertex& vertex);
    ~cVertex(void);
    size_t getId(void);
    eVertexColor getColor(void);
    double getValue(void);
    list <cEdge*>& getEdges(void);
    void setId(size_t vertexId);
    void setColor(const eVertexColor& color);
    void setValue(double newValue);
    void setEdges(const list <cEdge*>& newEdges);
    void add(size_t vertexId); 
    bool isPresent(size_t vertexId);  
    double getCost(size_t vertexId);
    void setCost(size_t vertexId, double newCost);
    void remove(size_t vertexId);
    void clear(void);
    string toString(const eVertexColor& color);
    friend bool operator== (cVertex& vertex1, cVertex& vertex2);
		friend bool operator== (cVertex& vertex, size_t vertexId);
		friend bool operator!= (cVertex& vertex1, cVertex& vertex2);
		friend bool operator!= (cVertex& vertex, size_t vertexId);
		friend ostream& operator<<(ostream& out, cVertex& vertex); 
};




#endif
