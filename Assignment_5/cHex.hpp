// Module for representing the hex game

#ifndef CHEX_HPP
#define CHEX_HPP

// Dependancies
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <chrono>
#include <algorithm> 
#include <random>
#include <time.h>
#include <array>
#include "cGraph.hpp"
#include "cMst.hpp"
#include "cPlayer.hpp"

// Namespace
using namespace std;
using std::chrono::system_clock;

// Constant representing the number of virtual vertices
const size_t NB_VIRTUAL_VERTICES = 4;

// Constant representing the number of players
const size_t NB_PLAYERS = 2;

// Constant representing the number of trials
const size_t NB_TRIALS = 1000;

// Class: cHex ISA cGraph
class cHex : public cGraph {
	
	private:
		size_t moves;
		size_t dimension;
		array <cPlayer, NB_PLAYERS> players;
		
	public:
		cHex(size_t dimension, ePlayerMode playerMode1, ePlayerMode playerMode2);
		cHex(const cHex& hex);
		~cHex(void);
		size_t getDimension(void);
		size_t getMoves(void);
		void play(void);
		
	private:
		void initializeVirtualVertex(size_t begin, size_t sentinel, size_t increment, eVertexColor color, size_t virtualVertex);
		void initialize(void);
		size_t determineHumanLocation(void);
		size_t determineAiLocation(size_t idPlayer);
		size_t next(size_t idPlayer);
		bool win(size_t move);
		void fillUp(const vector <size_t> remainingMoves, size_t& idPlayer);
		void rollback(vector <size_t> remainingMoves, size_t& idPlayer);
		void printRules(void);
		void printBoard(void);
		
		// Function determining if the vertex is located on the upper bound of the hex board
		inline bool isUpBound(size_t vertex){
			return (vertex / dimension == 0);
		};

		// Function determining if the vertex is located on the lower bound of the hex board
		inline bool isLowBound(size_t vertex){
			return (vertex / dimension == dimension - 1);
		};

		// Function determining if the vertex is located on the left bound of the hex board
		inline bool isLeftBound(size_t vertex){
			return (vertex % dimension == 0);
		};

		// Function determining if the vertex is located on the right bound of the hex board
		inline bool isRightBound(size_t vertex){
			return (vertex % dimension == dimension - 1);
		};

		// Function determining if the vertex is out of bound of the hex board
		inline bool isOutOfBound(size_t vertex){
			return (vertex >= dimension * dimension);
		};
		
		// Function determining if the vertex is virtual
		inline bool isVirtual(size_t vertex){
			return (vertex >= dimension * dimension) && (vertex <= dimension * dimension + 3);
		};
		
		// Function determining if the move is legal
		inline bool isLegal(size_t vertex){
			return ( (!isOutOfBound(vertex)) && (getNodeColor(vertex) == eVertexColor::WHITE) );
		};
		
		// Function getting the NORTH virtual vertex
		inline size_t getNorthVirtualVertex(void) {
			return dimension * dimension;
		};
				
		// Function getting the SOUTH virtual vertex
		inline size_t getSouthVirtualVertex(void) {
			return dimension * dimension + 1;
		};
		
		// Function getting the WEST virtual vertex
		inline size_t getWestVirtualVertex(void) {
			return dimension * dimension + 2;
		};
		
		// Function getting the EAST virtual vertex
		inline size_t getEastVirtualVertex(void) {
			return dimension * dimension + 3;
		};
		
		// Function converting a vertex location (move) to string
		inline string toString(size_t vertex){
			string result;
			result.append(1, vertex % dimension + 'A');
			result.append(1, vertex / dimension + '1');
			return result;
		}
};

#endif
