// Module for computing the shortest path thanks to the Dijkstra algorithm

#ifndef CSHORTESTPATH_CPP
#define CSHORTESTPATH_CPP

// Dependancies
#include <iostream>
#include <cmath>
#include <climits>
#include "cGraph.cpp"
#include "cPriorityQueue.cpp"

// Namespace
using namespace std;

// Class: cPriorityQueue
class cShortestPath {
  
  private:
    cGraph graph;
    cPriorityQueue queue;
    size_t source;
    vector<size_t> previous;   
    
  private:
    void applyDijkstra(size_t sourceId);
  
  public:
    cShortestPath(size_t nbNodes, double edgeDensity, double distanceMin, double distanceMax);
    ~cShortestPath(void);
    cGraph getGraph();
    cPriorityQueue getQueue();
    size_t getSource();
    vector<size_t>& getPrevious();
    void setGraph(cGraph& newGraph);
    void setQueue(cPriorityQueue& newQueue);
    void setSource(size_t newSource);
    void setPrevious(vector<size_t> newPreviousVertices);
    vector<cVertex*>& vertices(void);
    vector<cVertex*> path(size_t sourceId, size_t destinationId);
    double pathSize(size_t sourceId, size_t destinationId);
    double averagePathLength(size_t sourceId);
    void print(size_t sourceId);
};

// Contructor: create a new shortest path
cShortestPath::cShortestPath(size_t nbNodes, double edgeDensity, double distanceMin, double distanceMax){
  
  // Generate nodes
  graph.add(nbNodes);
  
  // Generate random edges
  graph.generateRandomEdges(edgeDensity, distanceMin, distanceMax);
  
  // Set the source vertex as not defined for the Dijkstra algorithm 
  source = UINT_MAX;
}

// Destructor: destroy the shortest path
cShortestPath::~cShortestPath(void){
}

// Accessor: get the graph
inline cGraph cShortestPath::getGraph(){
  return graph;
}

// Accessor: get the priority queue
inline cPriorityQueue cShortestPath::getQueue(){
  return queue;
}

// Accessor: get the source
inline size_t cShortestPath::getSource(){
  return source;
}

// Accessor: get the previous vertices
inline vector<size_t>& cShortestPath::getPrevious(){
  return previous;
}

// Mutator: set the graph
inline void cShortestPath::setGraph(cGraph& newGraph){
  graph = newGraph;
}

// Mutator: set the priority queue
inline void cShortestPath::setQueue(cPriorityQueue& newQueue){
  queue = newQueue;
}

// Mutator: set the source
inline void cShortestPath::setSource(size_t newSource){
  source = newSource;
}

// Mutator: set the previous vertices
inline void cShortestPath::setPrevious(vector<size_t> newPreviousVertices){
  previous = newPreviousVertices;
}

// Function returning the list of vertices in G(V,E)
inline vector<cVertex*>& cShortestPath::vertices(void){
  return graph.getVertices();
}

// Function applying Dijkstra algorithm from the source
void cShortestPath::applyDijkstra(size_t sourceId){

  // Get the number of vertices
  size_t nbVertices = graph.V();

  // Check U is not out of limit
  if (sourceId < nbVertices) {
    
    // Check if Dijkstra was not applied on the source
    if (source != sourceId){   
      
      // Set the source from which the algorith is applied
      source = sourceId;
      
      // Clear the previous vector
      previous.clear();
        
      // Add as many elements as vertices into the previous vector
      for (size_t i = 0; i < nbVertices; ++i){
        previous.push_back(UINT_MAX);
      }
          
      // Initialize the priority of all the vertices (except the source)
      for (size_t i = 0; i < nbVertices; ++i){
              
        // Check it is not the source
        if (i != sourceId){
          
          // Initialize the priority of the vertex
          graph.setNodeValue(i, INFINITY);
          
          // Insert the vertex into the queue
          queue.insert(vertices().at(i));
        }      
      }    
      
      // Set the priority of the first vertex of the path
      graph.setNodeValue(sourceId, 0);
      queue.insert(vertices().at(sourceId));
      
      // Continue while the queue is not empty
      while (queue.size() > 0){
              
        // Select the vertex whose the value is the lowest one
        cVertex * vertex = queue.minPriority();
        size_t vertexId = vertex->getId();
        
        // Get its neighbors
        list <cEdge*> neighbors = graph.neighbors(vertexId);
        
        // Update the value of its neighbors
        for (list<cEdge*>::iterator it = neighbors.begin(); it != neighbors.end(); ++it){
          cEdge * edge = *it;
          size_t neighborId = edge->getVertex();
          cVertex * neighbor = graph.getVertices().at(neighborId);
          
          // Evaluate the potential new value of the vertex
          double newPriority = vertex->getValue() + graph.getEdgeValue(vertexId, neighborId);
          
          // Compare this new value to the current one
          if ( newPriority < neighbor->getValue() ){   
            
            // Store the previous vertex id
            previous.at(neighborId) = vertexId;
                
            // Check this neighbor is still present into the heap
            if (queue.contains(neighborId)){
              
              // Change the priority of this neighbor
              queue.chgPriority(neighborId, newPriority);
            }
          }
        }
      }
    }
  }  
}

// Function finding shortest path between the source and the destination
// and returning the sequence of vertices representing it
vector<cVertex*> cShortestPath::path(size_t sourceId, size_t destinationId){
    
  // Declare a vector
  vector<cVertex*> pathFromSourceToDestination;
  
  // Get the number of vertices
  size_t nbVertices = graph.V();
  
  // Check U and W are not out of limit
  if ( (sourceId < nbVertices) and (destinationId < nbVertices) ){
    
    // Apply Dijkstra on the source
    applyDijkstra(sourceId);
    
    // Initialize the current vertex
    size_t current = destinationId;
    
    // Add the destination to the path
    cVertex * vertex = vertices().at(destinationId);
    pathFromSourceToDestination.push_back(vertex);
    
    // Continue while the current vertex is not the source
    bool loop = (current != sourceId);
    while (loop){
      
      // Get the id of the previous vertex
      current = previous.at(current);
            
      // Check this id is coherent
      if (current < nbVertices) {
            
        // Get the previous vertex 
        vertex = graph.getVertices().at(current);
        
        // Add the vertex to the path
        pathFromSourceToDestination.push_back(vertex);
      }
      
      // Shall we continue ?
      loop = (current != sourceId) and (current < nbVertices);
    }
    
    // Clear the path if the source is not reached
    if (current != sourceId){
      pathFromSourceToDestination.clear();
    }
  }
  
  // Return the path
  return pathFromSourceToDestination;
}

// Function return the path cost associated with the shortest path
inline double cShortestPath::pathSize(size_t sourceId, size_t destinationId){
  
  // Get the number of vertices
  size_t nbVertices = graph.V();
  
  // Check the source and the destination are not out of limit
  if ( (sourceId < nbVertices) and (destinationId < nbVertices) ){
    
    // Apply Dijkstra on the source
    applyDijkstra(sourceId);
  
    // Return the value of the destination
    return graph.getNodeValue(destinationId);
    
  } else {
    return 0;
  }
}

// Function return the average path length from the source to the other vertices
inline double cShortestPath::averagePathLength(size_t sourceId) {
  
  // Initialize the variables to compute the average path length
  double sumPathLengths = 0;
  size_t pathCounter = 0;
  
  // Get the number of vertices
  size_t nbVertices = graph.V();
  
  // Check the source is not out of limit
  if (sourceId < graph.V()) {
    
    // Apply Dijkstra on the source
    applyDijkstra(sourceId);
    
    // Compute the paths and theirs corresponding length
    for (size_t id = 0 ; id < nbVertices; id++){
    
      // Verify it is different from the source
      if (id != sourceId){
        
        // Get the length of the path
        double length = pathSize(sourceId, id);
        
        // Check the length is coherent
        if (length < INFINITY){
          
          // Compute the average path length
          pathCounter++;
          sumPathLengths += length;
        }
      }
    }
  }
  
  // Return the average path length
  if (pathCounter > 0){
    return sumPathLengths / pathCounter;
  } else {
    return 0;
  }
}

// Function printing the path and the path length from the source to the other vertices
void cShortestPath::print(size_t sourceId) {

  // Get the number of vertices
  size_t nbVertices = graph.V();
  
  // Check the source is not out of limit
  if (sourceId < nbVertices) {
    
    // Apply Dijkstra on the source
    applyDijkstra(sourceId);
    
    // Compute the paths and theirs corresponding length
    for (size_t id = 0 ; id < nbVertices; id++){
      
      // Verify it is different from the source
      if (id != sourceId){
      
        // Get the path between the source and id
        vector<cVertex*> pathFromSourceToId = path(sourceId, id); 
        
        // Verify that the path between the source and id exists
        if (!pathFromSourceToId.empty()){
        
          // Print the path
          cout << "Path from " << sourceId << " to " << id << ": ";
          for (int checkpoint = pathFromSourceToId.size() - 1 ; checkpoint >= 0 ; checkpoint--){
            cout << pathFromSourceToId.at(checkpoint)->getId() << " ";
          }
          cout << endl;
          
          // Get the length of the path
          double length = pathSize(sourceId, id);
          cout << "Length from " << sourceId << " to " << id << ": " << length << endl << endl;
        }
      }
    }
  }
}

#endif
