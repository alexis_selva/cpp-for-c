// Objective: implement a Monte Carlo simulation that calculates the average shortest path in a graph using:
//  * a pseudo-random number generator to produce edges and their costs
//  * the Dijkstra algorithm to compute the shortest path 

// Sources:
//  * http://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
//  * http://www.cplusplus.com
//  * http://www.cprogramming.com/tutorial/computersciencetheory/heap.html

// Explanation:
//  Given that the graphs generated during these simulations are sparse, I decided to represent the graph as 
//  an adjacency list. More concretly, I defined the graph (cf. cGraph.cpp module) as a vector of vertex pointers. 
//  Each vertex (cf. cVertex.cpp module) is represented by an id, a value (priority in the Dijkstra context) and a 
//  list of edge pointers. Each edge (cf. cEdge.cpp module) is defined by its cost and a remote vertex id.
//  For the priority queue (cf. cPriorityQueue.cpp module), I implemented a min heap which is efficient in the Dijkstra context.
//  To compute the shortest path, I implemented the Dijkstra algorithm (cf. cShortestPath module).
//  Then, this source code (simulation.cpp) makes it possible to put in place several Monte Carlo simulations that calculate
//  the average shortest path for two different edge densities (20% and 40%). Besides, according to the results 
//  (cf. output below), the average of the average shortest path length decreases when the edge density
//  increases. So, my conclusion is that increasing the number of potential routes reduces the path length between 
//  two nodes.

// Output:
//  Average of the average shortest path lengths for the simulation 1: 6.99625
//  Average of the average shortest path lengths for the simulation 2: 4.70252

// Dependancies
#include <iostream>
#include "cShortestPath.cpp"

// Namespace
using namespace std;

// Constante representing the number of tests per simulation
const size_t NUMBER_TESTS = 10000;

// Constante representing the number of nodes
const size_t NUMBER_NODES = 50;

// Constante representing the 1st density of edges
const double DENSITY_1 = 0.2;

// Constante representing the 2nd density of edges
const double DENSITY_2 = 0.4;

// Constante representing the distance min between 2 vertices
const double DISTANCE_MIN = 1.0;

// Constante representing the distance max between 2 vertices
const double DISTANCE_MAX = 10.0;

// Function launching Monte Carlo simulation
inline void simulation(size_t sim, double edgeDensity) {
  
  // Declare the average average path length
  double averageAverageLength = 0;
  
  // Compute the average of the average path lengths
  for (size_t test = 0; test < NUMBER_TESTS; test++){

    // Generate the environment
    cShortestPath path = cShortestPath(NUMBER_NODES, edgeDensity, DISTANCE_MIN, DISTANCE_MAX);
      
    // Get the average path length from the 1st node
    averageAverageLength += path.averagePathLength(0);
  }
    
  // Print the result of the simulation
  cout << "Average of the average shortest path lengths for the simulation " << sim << ": " << averageAverageLength / NUMBER_TESTS << endl;
}

// Main function
int main(){
  
  // Launch the simulation 1
  simulation(1, DENSITY_1);
  
  // Launch the simulation 2
  simulation(2, DENSITY_2);
  
  // Return OK
  return 0;
}
