// Module for representing a vertex

#ifndef CVERTEX_HPP
#define CVERTEX_HPP

// Dependancies
#include <iostream>
#include <list>
#include "cEdge.hpp"

// Namespace
using namespace std;

// Constant in case of unknown vertex
const double UNKNOWN = 0;

// List different color of vertex
enum eVertexColor { WHITE, BLUE, RED };

// Class: cVertex
class cVertex {
  
  private:
    size_t id;
		eVertexColor color;
    double value;
    list <cEdge*> edges;
  
  public:
    cVertex(size_t vertexId);
    ~cVertex(void);
    size_t getId(void);
    eVertexColor getColor(void);
    double getValue(void);
    list <cEdge*>& getEdges();
    void setId(size_t vertexId);
    bool setColor(eVertexColor color);
    void setValue(double newValue);
    void setEdges(list <cEdge*> newEdges);
    void add(size_t vertexId); 
    bool isPresent(size_t vertexId);  
    double getCost(size_t vertexId);
    void setCost(size_t vertexId, double newCost);
    void remove(size_t vertexId);
    void clear(void);
    void print(void);
    friend bool operator== (cVertex& vertex1, cVertex& vertex2);
		friend bool operator== (cVertex& vertex, size_t vertexId);
		friend bool operator!= (cVertex& vertex1, cVertex& vertex2);
		friend bool operator!= (cVertex& vertex, size_t vertexId);
		friend ostream& operator<<(ostream& out, cVertex& vertex); 
};




#endif
