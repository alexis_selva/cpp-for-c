// Module for representing an edge

// Dependancy
#include "cEdge.hpp"

// Constructor: create a new edge
cEdge::cEdge(size_t vertexId){
  cost = 0;
  vertex = vertexId;
}

// Destructor: destroy a edge
cEdge::~cEdge(void){
}

// Accessor: get the cost
double cEdge::getCost(void){
  return cost;
}

// Accessor: get the remote vertex
size_t cEdge::getVertex(void){
  return vertex;
}

// Mutator: modify the cost
void cEdge::setCost(double newCost){
  cost = newCost;
}

// Mutator: modify the remote vertex
void cEdge::setVertex(size_t newVertexId){
  vertex = newVertexId;
}

// Function overloading the operator ==
bool operator== (cEdge& edge1, cEdge& edge2){
  return (edge1.vertex == edge2.vertex);
}

// Function overloading the operator ==
bool operator== (cEdge& edge, size_t id){
  return (edge.vertex == id);
}
 
// Function overloading the operator !=
bool operator!= (cEdge& edge1, cEdge& edge2){
  return !(edge1 == edge2);
}
 
// Function overloading the operator !=
bool operator!= (cEdge& edge, size_t id){
  return !(edge == id);
}

// Function overloading the operator <<
ostream& operator<<(ostream& out, cEdge& edge){
  out.precision(DECIMALS);
  out << "\t----\t" << edge.cost << "\t----\t" << edge.vertex << endl;
  return out;
}

