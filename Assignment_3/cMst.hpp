// Module for computing the Minimum Spanning Tree

#ifndef CMST_HPP
#define CMST_HPP

// Dependancies
#include <iostream>
#include <fstream>
#include <tuple>
#include <queue>  
#include <numeric> 
#include "cGraph.hpp"

// Namespace
using namespace std;

// Definition of the tEdge
typedef pair <cVertex*, cEdge*> tPairVertexEdge;

// Definition of a tree
typedef vector <tPairVertexEdge> tTree;

// Class: cEdgeComparison
class cEdgeComparison {

  public:
		cEdgeComparison(void) {
		}
		inline bool operator() (const tPairVertexEdge& pair1, const tPairVertexEdge& pair2) const {
			return pair1.second->getCost() > pair2.second->getCost();
		}
};

// Definition of the heap
typedef priority_queue<tPairVertexEdge, tTree, cEdgeComparison> tMinHeap;

// Class: cMst
class cMst {

  private:
		cGraph* graph;		// Pointer of graph as member of cMst in order to use an existing graph
    tMinHeap queue;
    tTree tree;
    
  public:
	  cMst(void);
	  cMst(cGraph* graph);
  	~cMst(void);
  	cGraph* getGraph(void);
  	tTree& getTree(void);
  	void setGraph(cGraph* graph);
  	double getCost(void);
  	void applyPrim(size_t sourceId);
  	friend ostream& operator<<(ostream& out, cMst& mst);
};

#endif  
