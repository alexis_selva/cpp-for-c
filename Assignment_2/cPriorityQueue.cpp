// Module for evaluating the vertex with the next shortest link in the shortest path calculation

#ifndef CPRIORITYQUEUE_CPP
#define CPRIORITYQUEUE_CPP

// Dependancies
#include <iostream>
#include <vector>
#include "cVertex.cpp"

// Namespace
using namespace std;

// Class: cPriorityQueue
class cPriorityQueue {
  
  private:
    vector<cVertex*> heap;
    
  private:
    size_t parent(size_t index);
    size_t leftChild(size_t index);
    size_t rightChild(size_t index);
    void shiftUp(size_t index);
    void shiftDown(size_t index);
    
  public:
    cPriorityQueue(void);
    ~cPriorityQueue(void);
    vector<cVertex*>& getHeap(void);
    void setHeap(vector<cVertex*> newHeap);
    void chgPriority(size_t vertex, double priority);
    cVertex* minPriority(void);
    bool contains(size_t vertex);
    void insert(cVertex* vertex);
    cVertex* top(void);
    size_t size(void);
    void print(void);
};

// Contructor: create a new priorityQueue
cPriorityQueue::cPriorityQueue(void){
}

// Destructo: destroy the priorityQueue
cPriorityQueue::~cPriorityQueue(void){
  heap.clear();
}

// Accessor: get the heap
inline vector<cVertex*>& cPriorityQueue::getHeap(void){
  return heap;
}

// Mutator: set the the heap
inline void cPriorityQueue::setHeap(vector<cVertex*> newHeap){
  heap = newHeap;
}

// Function returning the index of the parent
inline size_t cPriorityQueue::parent(size_t index){
  return (index - 1) / 2;
}

// Function returning the index of the left child 
inline size_t cPriorityQueue::leftChild(size_t index){
  return index * 2 + 1;
}

// Function returning the index of the right child 
inline size_t cPriorityQueue::rightChild(size_t index){
  return index * 2 + 2;
}

// Function changing the priority (node value) of an element
inline void cPriorityQueue::chgPriority(size_t vertexId, double priority){
  cVertex* vertex = NULL;
  bool found = false;
  size_t i = 0;
  bool loop = i < heap.size();
    
  while (loop){
        
    // Get the vertex (reference) 
    vertex = heap.at(i);
    
    // Is it the one we are looking for ?
    found = (*vertex == vertexId);
    
    // Get the next 
    i++;
    
    // Shall we continue ?
    loop = (i < heap.size()) and (!found);
  }
  
  // Update the vertex
  if (found){
        
    // Set its priority
    vertex->setValue(priority);
    
    // Update the queue from its position to the root
    shiftUp(i - 1);
  }
}

// Function removing the top element of the queue 
inline cVertex* cPriorityQueue::minPriority(void){
  
  // Get the min of the heap
  cVertex* min = heap.at(0);
  
  // Replace the min with the last element of the heap
  heap.at(0) = heap.at(heap.size() - 1);  
  
  // Remove the last element of the heap
  heap.pop_back();
  
  // Recompute the min of the heap from the root
  shiftDown(0);
  
  return min;
}

// Function determining if the queue contains a vertex
inline bool cPriorityQueue::contains(size_t vertexId){
  
  vector<cVertex*>::iterator it = heap.begin();
  bool found = false;
  bool loop = it != heap.end();
  while (loop){
    
    // Get the vertex (reference) 
    cVertex* vertex = *it;
    
    // Is it the one we are looking for ?
    found = (*vertex == vertexId);
    
    // Get the next 
    ++it;
    
    // Shall we continue ?
    loop = (it!=heap.end()) and (!found);
  }
  
  return found;
}


// Function inserting an element into queue
inline void cPriorityQueue::insert(cVertex* vertex){
  
  // Insert a vertex at the end of the vector
  heap.push_back(vertex);
  
  // Recompute the min of the heap from the tail
  shiftUp(heap.size() - 1);  
}

// Function returning the top element of the queue
inline cVertex* cPriorityQueue::top(void){
  
  // Get the min of the heap
  cVertex* min = heap.at(0);
  return min;
}

// Function returning the number of elements in the queue
inline size_t cPriorityQueue::size(void){
  return heap.size();
}

// Function updating the heap from the index to the root 
void cPriorityQueue::shiftUp(size_t index){
    
  // Check it is not the root
  if (index > 0){
    
    // Get its parent index
    size_t parentIndex = parent(index);

    // Check that the parent is greater than its child at index
    if (heap.at(parentIndex)->getValue() > heap.at(index)->getValue()){
      
      // Replace the parent position and the one of its child
      cVertex* tmp = heap.at(parentIndex);
      heap.at(parentIndex) = heap.at(index);
      heap.at(index) = tmp;
    }
    
    // Continue the treatment from its parent
    shiftUp(parentIndex);
  }
}

// Function updating the heap from the index to the leaf
void cPriorityQueue::shiftDown(size_t index){
  
  // Get the heap size
  size_t heapSize = heap.size();
  
  // Check the index is not over limit
  if (index < heapSize){
  
    // Get the left child
    size_t childIndex = leftChild(index);    
      
    // Is the value of the child greather than the one of its parent ?
    if ( (childIndex < heapSize) and (heap.at(index)->getValue() > heap.at(childIndex)->getValue()) ) {
      
      // Replace the parent position and the one of its child
      cVertex* tmp = heap.at(index);
      heap.at(index) = heap.at(childIndex);
      heap.at(childIndex) = tmp;
      
      // Continue the treatment from its child
      shiftDown(childIndex);
    }
    
    // Get the right child
    childIndex = rightChild(index);    
      
    // Is the value of the child greather than the one of its parent ?
    if ( (childIndex < heapSize) and (heap.at(index)->getValue() > heap.at(childIndex)->getValue()) ) {
      
      // Replace the parent position and the one of its child
      cVertex* tmp = heap.at(index);
      heap.at(index) = heap.at(childIndex);
      heap.at(childIndex) = tmp;
      
      // Continue the treatment from its child
      shiftDown(childIndex);
    }
  }
}

// Function printing the graph to stdout 
inline void cPriorityQueue::print(void){
  cout << "Heap:" << endl;
  for (vector<cVertex*>::iterator it = heap.begin(); it != heap.end(); ++it){
    cVertex* vertex = *it;
    cout << vertex->getId() << ": " << vertex->getValue() << endl;
  }
  cout << endl;
}

#endif
