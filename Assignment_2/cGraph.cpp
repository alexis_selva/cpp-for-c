// Module for representing a graph

#ifndef CGRAPH_CPP
#define CGRAPH_CPP

// Dependancies
#include <iostream>
#include <vector>
#include <stdlib.h> 
#include <time.h>
#include <cmath>
#include "cVertex.cpp"

// Namespace
using namespace std;

// Constante representing the default number of vertices
const size_t DEFAULT_NUMBER_VERTICES = 50;

// Class: cGraph
class cGraph {
  
  private:
    vector<cVertex*> vertices;
    size_t nbEdges;           
  
  public:
    cGraph(void);
    ~cGraph(void);
    vector<cVertex*>& getVertices(void);
    size_t getNbEdges(void);
    void setVertices(vector<cVertex*> vertices);
    void setNbEdges(size_t newNbEdges);
    size_t V(void);
    size_t E(void);
    bool adjacent(size_t vertexX, size_t vertexY);
    list<cEdge*> neighbors(size_t vertexX);
    void add(size_t nb);
    void add(size_t vertexX, size_t vertexY);
    bool isPresent(size_t vertex);
    void remove(size_t vertexX, size_t vertexY);
    double getNodeValue(size_t vertex);
    void setNodeValue(size_t vertex, double a);
    double getEdgeValue(size_t vertexX, size_t vertexY);
    void setEdgeValue(size_t vertexX, size_t vertexY, double cost);
    void generateRandomEdges(double edgeDensity, double distanceMin, double distanceMax);
    void print(void);
};

// Contructor: create a new graph with a particular number of nodes
cGraph::cGraph(void){
  nbEdges = 0;
}

// Destructor: destroy the graph
cGraph::~cGraph(void){
  cVertex* vertex = NULL;
  
  while (!vertices.empty()){
    
    // Get the last vertex
    vertex = vertices.back();
    
    // Remove the last vertex from the vector
    vertices.pop_back();
    
    // Clear the edges of the vertex
    vertex->clear();
    
    // Delete the vertex forever
    delete(vertex);
    vertex = NULL;    
  }
}

// Accessor: get the vertices
inline vector<cVertex*>& cGraph::getVertices(void){
  return vertices;
}

// Accessor: get the number of edges
inline size_t cGraph::getNbEdges(void){
  return nbEdges;
}

// Mutator: set the vertices
inline void cGraph::setVertices(vector<cVertex*> newVertices){
  vertices = newVertices;
}

// Mutator: set the number of edges
inline void cGraph::setNbEdges(size_t newNbEdges){
  nbEdges = newNbEdges;
}

// Function returning the number of vertices in the graph
inline size_t cGraph::V(void){
  return vertices.size();
}

// Function returning the number of edges in the graph
inline size_t cGraph::E(void){
  return nbEdges;
}

// Function testing whether there is an edge from node x to node y
inline bool cGraph::adjacent(size_t vertexX, size_t vertexY){
  
  // Verify vertices exist
  if (vertices.empty()){
    return false;
  } else {
    return vertices.at(vertexX)->isPresent(vertexY);
  }
}

// Function listing all nodes y such that there is an edge from x to y.
inline list <cEdge*> cGraph::neighbors(size_t vertexX){
  return vertices.at(vertexX)->getEdges();
}

// Function adding a particular number of vertices to the graph
inline void cGraph::add(size_t nb = DEFAULT_NUMBER_VERTICES){
  for (size_t i = 0 ; i < nb ; ++i){
    vertices.push_back(new cVertex(i));
  }
}

//Function adding the edge from x to y, if it is not there
inline void cGraph::add(size_t vertexX, size_t vertexY){
  
  // Get the nuber of vertices
  size_t nbVertices = vertices.size();
  
  // Check X and Y are correct
  if ((vertexX < nbVertices) and (vertexY < nbVertices)){
    
    // Add the edge between X and Y
    vertices.at(vertexX)->add(vertexY);
    
    // Verify it is not a self-loop ?
    if (vertexX != vertexY){
      
      // Add the edge between Y and X
      vertices.at(vertexY)->add(vertexX);
    }
  
    // Increment the number of edges
    nbEdges++;
  }
}

// Function checking if a vertex is present
inline bool cGraph::isPresent(size_t vertex){
  return (vertex < vertices.size());
}

//Function removing the edge from x to y, if it is there.
inline void cGraph::remove(size_t vertexX, size_t vertexY){
       
  // Evaluate the current size of the vertex vector
  size_t nbVertices = vertices.size();
  
  // Check X and Y are correct
  if ((vertexX < nbVertices) and (vertexY < nbVertices)){
    
    // Remove the edge between X and Y
    vertices.at(vertexX)->remove(vertexY);
    
    // Verify it is not a self-loop ?
    if (vertexX != vertexY){
      
      // Remove the edge between Y and X
      vertices.at(vertexY)->remove(vertexX);
    } 
    
    // Decrement the number of edges
    nbEdges--;
  }  
}

//Function returning the priority associated to the node X
inline double cGraph::getNodeValue(size_t vertex){
  
  // Evaluate the current size of the vertex vector
  size_t nbVertices = vertices.size();
  
  // Check X is correct
  if (vertex < nbVertices){
    
    // Get the value of the vertex
    return vertices.at(vertex)->getValue();
    
  } else {
      
    // Unknown vertex ?
    return UNKNOWN;
  }
}  

// Function setting the value associated to the node X
inline void cGraph::setNodeValue(size_t vertex, double value){
  
  // Evaluate the current size of the vertex vector
  size_t nbVertices = vertices.size();

  // Check X is correct
  if (vertex < nbVertices){
       
    // Change the value of the vertex X
    vertices.at(vertex)->setValue(value);
  } 
}

// Function returning the value associated to the edge (x,y)
inline double cGraph::getEdgeValue(size_t vertexX, size_t vertexY){
    
  // Evaluate the current size of the vertex vector
  size_t nbVertices = vertices.size();
  
  // Check X and Y are correct
  if ((vertexX < nbVertices) and (vertexY < nbVertices)){
    
    // Return the cost of the edge between X and Y
    return vertices.at(vertexX)->getCost(vertexY);
     
  } else { 
    
    // Unknown vertices ?
    return UNKNOWN;
  }
}

// Function setting the value associated to the edge (x,y) to v
inline void cGraph::setEdgeValue(size_t vertexX, size_t vertexY, double cost){
    
  // Evaluate the current size of the vertex vector
  size_t nbVertices = vertices.size();
  
  // Check X and Y are correct
  if ((vertexX < nbVertices) and (vertexY < nbVertices)){
      
    // Set the cost of the edge between X and Y
    vertices.at(vertexX)->setCost(vertexY, cost);  
      
    //Set the cost of the edge between Y and X
    vertices.at(vertexY)->setCost(vertexX, cost);
  }
}

// Function generating random edges
void cGraph::generateRandomEdges(double edgeDensity, double distanceMin, double distanceMax){
    
  // Get the number of vertices
  size_t nbVertices = V();
  
  // Initialize seed
  srand(clock());

  // List the potential source vertex (X) of an edge
  for (size_t vertexX = 0 ; vertexX < nbVertices ; vertexX++){
    
    // List the potential destination vertex (Y) of an edge
    for (size_t vertexY = vertexX + 1 ; vertexY < nbVertices ; vertexY++){
      
      // Compute a random number between 0 and 1
      double random = (static_cast<double>(rand()) / static_cast<double>(RAND_MAX));
      
      // Verify that a edge shall exist between X and Y
      if (random < edgeDensity){
        
        // Add an edge between X and Y
        add(vertexX, vertexY); 
         
        // Generate a random distance
        double distance = (distanceMax - distanceMin) * rand() / RAND_MAX + distanceMin;
        
        // Set the distance of the edge
        setEdgeValue(vertexX, vertexY, distance);
      }
    }
  }
}

// Function printing the graph to stdout 
inline void cGraph::print(void){
  cout << "Graph:" << endl;
  for (vector<cVertex*>::iterator it = vertices.begin(); it != vertices.end(); ++it){
    cVertex* vertex = *it;
    cout << *vertex;
  }
  cout << endl;
}

#endif
