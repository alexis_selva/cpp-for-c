// Module for representing the hex game

// Dependancies
#include "cHex.hpp"

// Constructor: generate the hex board game
cHex::cHex(size_t dimension){
	
	// Set the number of moves
	moves = 0;
	
	// Memorize the dimension
	this->dimension = dimension;
	
	// Initialize the board the edges between the vertices
	initialize();
}

// Destructor: destroy the hex board game
cHex::~cHex(void){
}

// Accessor: return the dimension of the board game
size_t cHex::getDimension(void){
	return dimension;
}

// Accessor: return the number of moves 
size_t cHex::getMoves(void){
	return moves;
}

// Accessor: return the board game
cGraph& cHex::getBoard(void){
	return board;
}

// Function nitializing the virtual vertex by adding edges between it and the nodes of the corresponding border
void cHex::initializeVirtualVertex(size_t begin, size_t sentinel, size_t increment, eVertexColor color, size_t virtualVertex){
	for (size_t source = begin; source < sentinel; source += increment){
		board.add(source, virtualVertex);
	}
	board.setNodeColor(virtualVertex, color);
}

// Function initializing the board
void cHex::initialize(void){
	
	// Add the vertices into the graph
	board.add(dimension * dimension + NB_VIRTUAL_VERTICES);
	
	// Get the number of non virtual vertices
  size_t nbVertices = board.V() - NB_VIRTUAL_VERTICES;
  
  // Create the edges between the vertices
	for (size_t vertex = 0 ; vertex < nbVertices ; vertex++){
		
		// Determine the location (upper / lower / intern) of the vertex
		if (isUpBound(vertex)){
			
			// Determine the location (left / right / intern) of the vertex
			if (isLeftBound(vertex)){
				board.add(vertex, vertex + 1);
				board.setEdgeValue(vertex, vertex + 1, 1);
				board.add(vertex, vertex + dimension);	
				board.setEdgeValue(vertex, vertex + dimension, 1);
			} else if (isRightBound(vertex)){
				board.add(vertex, vertex + dimension - 1);
				board.setEdgeValue(vertex, vertex + dimension - 1, 1);
				board.add(vertex, vertex + dimension);
				board.setEdgeValue(vertex, vertex + dimension, 1);
			} else {
				board.add(vertex, vertex + 1);
				board.setEdgeValue(vertex, vertex + 1, 1);
				board.add(vertex, vertex + dimension - 1);
				board.setEdgeValue(vertex, vertex + dimension - 1, 1);
				board.add(vertex, vertex + dimension);
				board.setEdgeValue(vertex, vertex + dimension, 1);
			}
			
		} else if (isLowBound(vertex)){
			
			// Determine the location (left / right / intern) of the vertex
			if (isLeftBound(vertex)){
				board.add(vertex, vertex + 1);
				board.setEdgeValue(vertex, vertex + 1, 1);
			} else if (isRightBound(vertex)){
				; //nop
			} else {
				board.add(vertex, vertex + 1);
				board.setEdgeValue(vertex, vertex + 1, 1);
			}
			
		} else {
			
			// Determine the location (left / right / intern) of the vertex
			if (isLeftBound(vertex)){
				board.add(vertex, vertex + 1);
				board.setEdgeValue(vertex, vertex + 1, 1);
				board.add(vertex, vertex + dimension);	
				board.setEdgeValue(vertex, vertex + dimension, 1);
			} else if (isRightBound(vertex)){
				board.add(vertex, vertex + dimension - 1);
				board.setEdgeValue(vertex, vertex + dimension - 1, 1);
				board.add(vertex, vertex + dimension);
				board.setEdgeValue(vertex, vertex + dimension, 1);
			} else {
				board.add(vertex, vertex + 1);
				board.setEdgeValue(vertex, vertex + 1, 1);
				board.add(vertex, vertex + dimension - 1);
				board.setEdgeValue(vertex, vertex + dimension - 1, 1);
				board.add(vertex, vertex + dimension);
				board.setEdgeValue(vertex, vertex + dimension, 1);
			}
		}
	}
	
	// Initialize the virtual vertices
	initializeVirtualVertex(0, dimension * dimension - dimension, dimension, eVertexColor::BLUE, getEastVirtualVertex());
	initializeVirtualVertex(dimension - 1, dimension * dimension, dimension, eVertexColor::BLUE, getWestVirtualVertex());
	initializeVirtualVertex(0, dimension, 1, eVertexColor::RED, getNorthVirtualVertex());
	initializeVirtualVertex(dimension * dimension - dimension, dimension * dimension, 1, eVertexColor::RED, getSouthVirtualVertex());
}

// Function allowing to set the color to the next location
void cHex::next(eVertexColor color){
			
	// Print a description
	cout << "Move " << moves + 1;
	if (color == eVertexColor::BLUE){
		cout << ": Human(X)" << endl;
	} else if (color == eVertexColor::RED){
		cout << ": Human(0)" << endl;
	}
			
	// Declare a setOK variable
	bool setOK(true);
	
	// Display the menu and return a correct answer
	do {
				
		// Declare the vertex id
		size_t vertex(0);
		
		// Display a message
		cout << "Enter location: ";
		
		// Declare the location
		string answer;
		
		// Read the answer from stdin
		cin >> answer;
		
		// Extract the different parts of the answer
		size_t part1 = static_cast<size_t>(answer.at(0) - 'A');
		size_t part2 = 0;
		try {
			part2 = stoi(answer.substr(1)) - 1;
			setOK = part1 >= 0 and part1 < dimension and part2 >= 0 and part2 < dimension;
		} catch (const invalid_argument& arg){
			setOK = false;
		}
		
		// Check the different parts of the answer
		if (setOK){
			
			// Determine the corresponding vertex
			vertex = part2 * dimension + part1;

			// Update the retry variable
			setOK &= !isOutOfBound(vertex);
		}
		
		// Check the vertex is valid
		if (!setOK){
			cout << "ERROR - Invalid position!" << endl;
		} else {
			
			// Set the color of the vertex
			setOK &= board.setNodeColor(vertex, color);
			
			// Check the modification of the color
			if (!setOK){
				cout << "ERROR - Location already occupied!" << endl;
			}
		}
		
	} while (!setOK);
	cout << "--------------------------------------------------------------------------------" << endl;
	
	// Increment the number of moves
	moves++;
}

// Function determining if a color wins the game
bool cHex::win(eVertexColor color){
	
	// Initialize MST with the board
	cMst mst (&board);
	
	// Apply Prim's algorithm from one of the virtual vertex
	if (color == eVertexColor::BLUE) {
		mst.applyPrim(getWestVirtualVertex());
	} else {
		mst.applyPrim(getNorthVirtualVertex());
	}
	
	// Determine the MST
	tTree tree = mst.getTree();
				
	// Get all edges of the MST
	for (auto pair : tree){
		
		// Get the edge
		cEdge * edge = pair.second;
						
		// Determine if the remote vertex of the edge is the expected virtual vertex
		if (color == eVertexColor::BLUE) {
			size_t virtualVertex = getEastVirtualVertex();
			if (virtualVertex == edge->getVertex()) {
				cout << "Blue human (X) wins" << endl;
				cout << "--------------------------------------------------------------------------------" << endl;
				return true;
			}
		} else if (color == eVertexColor::RED){
			size_t virtualVertex = getSouthVirtualVertex();
			if (virtualVertex == edge->getVertex()) {
				cout << "Red human (0) wins" << endl;
				cout << "--------------------------------------------------------------------------------" << endl;
				return true;
			}
		}  
	}

	return false;
}

// Function managing the game
void cHex::play(void){
		
	// Display the rules
	printRules();
	
	// Display the board
	printBoard();
	
	// Declare a color
	eVertexColor color(eVertexColor::WHITE);
	
	// Repeat until the game is finished
	do {
		
		// Set the color
		if (color == eVertexColor::BLUE){
			color = eVertexColor::RED;
		} else {
			color = eVertexColor::BLUE;
		}
		
		// Set the next color
		next(color);
		
		// Display the board
		printBoard();
		
	} while (!win(color));
}

// Function printing the rules
void cHex::printRules(void){
	cout << "RULES:" << endl;
	cout << " * Blue Human location 	(X) => objective: start the game and connect WEST-EAST" << endl;
	cout << " *  Red Human location 	(0) => objective: connect NORTH-SOUTH" << endl;
	cout << " *      Empty location 	(.)" << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
}

// Function printing the board recursively
void cHex::printBoard(void){
	
	// Print description
	if (moves > 0){
		cout << "BOARD (after move " << moves << ")" << endl;
	} else {
		cout << "BOARD (no moves)" << endl;
	}

	// Print the NORTH border
	cout << endl << "   ";
	for (char legend = 'A' ; legend < 'A' + dimension ; legend++){
		cout << legend << "   ";
	}
	
	// Declare the lines to print
	string currentLine;
	string nextLine;
	
	// Declare the legend to print
	size_t legend = 0;
		
	// Get all non-virtual vertices
	for (size_t vertexId = 0 ; vertexId < board.V() - NB_VIRTUAL_VERTICES ; vertexId++) {
		
		// Update the lines if necessary
		if (isLeftBound(vertexId)){
			legend++;
			if (legend < 10){
				currentLine.append((vertexId / dimension + 1) * 2 , ' ');
			} else {
				currentLine.append((vertexId / dimension + 1) * 2 - 1 , ' ');
			}
			stringstream stream;
			stream << legend;
			currentLine.append(stream.str());
			currentLine.append(" ");
			nextLine.append((vertexId / dimension + 2) * 2 + 1, ' ');
		}
		
		// Get the vertex
		cVertex* vertex = board.getVertices().at(vertexId);
		
		// Print the vertex color
		if (vertex->getColor() == eVertexColor::BLUE){
			currentLine.append("X");
		} else if (vertex->getColor() == eVertexColor::RED){
			currentLine.append("0");
		} else {
			currentLine.append(".");
		}
		
		// Get the edges of the vertex
		for (auto edge : vertex->getEdges()){
			
			// Determine if it is the next vertex
			if ( (edge->getVertex() > vertexId) and (!isVirtual(edge->getVertex())) ) {
				
				// Determine if it is the current line or the next one
				if (edge->getVertex() == vertexId + 1){
					currentLine.append(" _ ");
				} else if (edge->getVertex() == vertexId + dimension - 1){
					nextLine.append("/ ");
				} else if (edge->getVertex() == vertexId + dimension){
					nextLine.append("\\ ");
				}
			}
		}
		
		// Determine the vertex is located on the right bound
		if (isRightBound(vertexId)){
			
			// Print the legend
			currentLine.append(" ");
			stringstream stream;
			stream << legend;
			currentLine.append(stream.str());
					
			// Print the current line and the next one
			cout << endl << currentLine << endl << nextLine;
			
			// Clear the current line 
			currentLine.clear();
			
			// Clear the next line
			nextLine.clear();
		} 
	}
	
	// Print the SOUTH border
	cout << endl;
	currentLine.clear();
	currentLine.append((dimension + 1) * 2 + 1, ' ');
	cout << currentLine;
	for (char legend = 'A' ; legend < 'A' + dimension ; legend++){
		cout << legend << "   ";
	}
	cout << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
}
