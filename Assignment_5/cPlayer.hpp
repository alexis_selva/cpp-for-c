// Module for representing a player

#ifndef CPLAYER_HPP
#define CPLAYER_HPP

// Dependancies
#include <iostream>
#include "cVertex.hpp"

// Namespace
using namespace std;

// Alias of vertex colors
typedef eVertexColor ePlayerColor;

// List different mode of a player
enum class ePlayerMode : char { AI, HUMAN };

// Class: cPlayer
class cPlayer {
	
	private:
		ePlayerColor color;
		ePlayerMode mode;
		
	public:
		cPlayer(const ePlayerColor& color = ePlayerColor::BLUE, const ePlayerMode& mode = ePlayerMode::AI);
		~cPlayer(void);
		ePlayerColor getColor(void) const;
		ePlayerMode getMode(void) const;
		void setColor(const ePlayerColor& color);
		void setMode(const ePlayerMode& mode);
		friend ostream& operator<<(ostream& out, cPlayer& player);
		
		// Function determining if the player is human
		inline bool isHuman(void){
			return (mode == ePlayerMode::HUMAN);
		};		
};

#endif
