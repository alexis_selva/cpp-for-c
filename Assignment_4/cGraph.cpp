// Module for representing a graph

// Dependancy
#include "cGraph.hpp"

// Contructor: create an empty graph
cGraph::cGraph(void){
  nbEdges = 0;
}

// Contructor: create a new graph according to an input data file
cGraph::cGraph(char * inputFile){
	
	// Initialize the number of edges
	nbEdges = 0;

	// Declare the number of vertices
	size_t nbVertices(0);
	
	//Open the input data file
	ifstream inputStream(inputFile);
	
	// Check the input data file is correct
	if (inputStream.good()) {

		// Read the number of vertices from the input data file
		getNbVertices(nbVertices, inputStream);
		
		// Verify it is a correct line
		if (nbVertices > 0){

			// Add the vertices
			add(nbVertices);
			
			// Continue while it is not the end of file
			while (!inputStream.eof()){
				
				// Declare the 2 vertex ids and a cost 
				size_t vertexX(0);
				size_t vertexY(0);
				double distance(0.0);
				
				// Read the edge and its corresponding cost from the input data file
				getEdge(vertexX, vertexY, distance, inputStream);
				
				// Verify it is a correct line 
				if ((vertexX > 0) and (vertexX > 0) and (distance > 0)){ 
				
					// Add the edge
					add(vertexX, vertexY); 
							
					// Set the distance of the edge
					setEdgeValue(vertexX, vertexY, distance);
				}
			}
		}
		
		// Close the input data file
		inputStream.close();
		
	} else {
		
		// Print an explicative message
		cout << "Error while reading the input file" << endl;
	}
}

// Destructor: destroy the graph
cGraph::~cGraph(void){

  while (!vertices.empty()){
    
    // Get the last vertex
    cVertex* vertex = vertices.back();
    
    // Remove the last vertex from the vector
    vertices.pop_back();
    
    // Clear the edges of the vertex
    vertex->clear();
    
    // Delete the vertex forever
    delete(vertex);
    vertex = nullptr;    
  }
}

// Accessor: get the vertices
vector<cVertex*>& cGraph::getVertices(void){
  return vertices;
}

// Accessor: get the number of edges
size_t cGraph::getNbEdges(void){
  return nbEdges;
}

// Mutator: set the vertices
void cGraph::setVertices(vector<cVertex*> newVertices){
  vertices = newVertices;
}

// Mutator: set the number of edges
void cGraph::setNbEdges(size_t newNbEdges){
  nbEdges = newNbEdges;
}

// Function getting the number of vertices from an input data file 
void cGraph::getNbVertices(size_t& nbVertices, ifstream& inputStream){
	inputStream >> nbVertices;
}

// Function getting the vertices linked by an edge and the correspding distance from an input data file 
void cGraph::getEdge(size_t& vertexX, size_t& vertexY, double& distance, ifstream& inputStream){
	inputStream >> vertexX >> vertexY >> distance;
}

// Function returning the number of vertices in the graph
size_t cGraph::V(void){
  return vertices.size();
}

// Function returning the number of edges in the graph
size_t cGraph::E(void){
  return nbEdges;
}

// Function testing whether there is an edge from node x to node y
bool cGraph::adjacent(size_t vertexX, size_t vertexY){
  
  // Verify vertices exist
  if (vertices.empty()){
    return false;
  } else {
    return vertices.at(vertexX)->isPresent(vertexY);
  }
}

// Function listing all nodes y such that there is an edge from x to y.
list <cEdge*> cGraph::neighbors(size_t vertexX){
  return vertices.at(vertexX)->getEdges();
}

// Function adding a particular number of vertices to the graph
void cGraph::add(size_t nb = DEFAULT_NUMBER_VERTICES){
  for (size_t i = 0 ; i < nb ; ++i){
    vertices.push_back(new cVertex(i));
  }
}

//Function adding the edge from x to y, if it is not there
void cGraph::add(size_t vertexX, size_t vertexY){
  
  // Get the nuber of vertices
  size_t nbVertices = vertices.size();
  
  // Check X and Y are correct
  if ((vertexX < nbVertices) and (vertexY < nbVertices)){
    
    // Add the edge between X and Y
    vertices.at(vertexX)->add(vertexY);
    
    // Verify it is not a self-loop ?
    if (vertexX != vertexY){
      
      // Add the edge between Y and X
      vertices.at(vertexY)->add(vertexX);
    }
  
    // Increment the number of edges
    nbEdges++;
  }
}

// Function checking if a vertex is present
bool cGraph::isPresent(size_t vertex){
  return (vertex < vertices.size());
}

//Function removing the edge from x to y, if it is there.
void cGraph::remove(size_t vertexX, size_t vertexY){
       
  // Evaluate the current size of the vertex vector
  size_t nbVertices = vertices.size();
  
  // Check X and Y are correct
  if ((vertexX < nbVertices) and (vertexY < nbVertices)){
    
    // Remove the edge between X and Y
    vertices.at(vertexX)->remove(vertexY);
    
    // Verify it is not a self-loop ?
    if (vertexX != vertexY){
      
      // Remove the edge between Y and X
      vertices.at(vertexY)->remove(vertexX);
    } 
    
    // Decrement the number of edges
    nbEdges--;
  }  
}

//Function returning the color associated to the vertex
eVertexColor cGraph::getNodeColor(size_t vertex){
  
  // Evaluate the current size of the vertex vector
  size_t nbVertices = vertices.size();
  
  // Check X is correct
  if (vertex < nbVertices){
    
    // Get the color of the vertex
    return vertices.at(vertex)->getColor();
    
  } else {
      
    // Unknown vertex ?
    return eVertexColor::WHITE;
  }
}  

// Function setting the color associated to the vertex
bool cGraph::setNodeColor(size_t vertex, eVertexColor color){
  
  // Evaluate the current size of the vertex vector
  size_t nbVertices = vertices.size();

  // Check X is correct
  if (vertex < nbVertices){
       
    // Change the value of the vertex X
    return vertices.at(vertex)->setColor(color);
  } 
  
  // Return an error
  return false;
}

//Function returning the priority associated to the vertex
double cGraph::getNodeValue(size_t vertex){
  
  // Evaluate the current size of the vertex vector
  size_t nbVertices = vertices.size();
  
  // Check X is correct
  if (vertex < nbVertices){
    
    // Get the value of the vertex
    return vertices.at(vertex)->getValue();
    
  } else {
      
    // Unknown vertex ?
    return UNKNOWN;
  }
}  

// Function setting the value associated to the vertex
void cGraph::setNodeValue(size_t vertex, double value){
  
  // Evaluate the current size of the vertex vector
  size_t nbVertices = vertices.size();

  // Check X is correct
  if (vertex < nbVertices){
       
    // Change the value of the vertex X
    vertices.at(vertex)->setValue(value);
  } 
}

// Function returning the value associated to the edge (x,y)
double cGraph::getEdgeValue(size_t vertexX, size_t vertexY){
    
  // Evaluate the current size of the vertex vector
  size_t nbVertices = vertices.size();
  
  // Check X and Y are correct
  if ((vertexX < nbVertices) and (vertexY < nbVertices)){
    
    // Return the cost of the edge between X and Y
    return vertices.at(vertexX)->getCost(vertexY);
     
  } else { 
    
    // Unknown vertices ?
    return UNKNOWN;
  }
}

// Function setting the value associated to the edge (x,y) to cost
void cGraph::setEdgeValue(size_t vertexX, size_t vertexY, double cost){
    
  // Evaluate the current size of the vertex vector
  size_t nbVertices = vertices.size();
  
  // Check X and Y are correct
  if ((vertexX < nbVertices) and (vertexY < nbVertices)){
      
    // Set the cost of the edge between X and Y
    vertices.at(vertexX)->setCost(vertexY, cost);  
      
    //Set the cost of the edge between Y and X
    vertices.at(vertexY)->setCost(vertexX, cost);
  }
}

// Function generating random edges
void cGraph::generateRandomEdges(double edgeDensity, double distanceMin, double distanceMax){
    
  // Get the number of vertices
  size_t nbVertices = V();
  
  // Initialize seed
  srand(clock());

  // List the potential source vertex (X) of an edge
  for (size_t vertexX = 0 ; vertexX < nbVertices ; vertexX++){
    
    // List the potential destination vertex (Y) of an edge
    for (size_t vertexY = vertexX + 1 ; vertexY < nbVertices ; vertexY++){
      
      // Compute a random number between 0 and 1
      double random = (static_cast<double>(rand()) / static_cast<double>(RAND_MAX));
      
      // Verify that a edge shall exist between X and Y
      if (random < edgeDensity){
        
        // Add an edge between X and Y
        add(vertexX, vertexY); 
         
        // Generate a random distance
        double distance = (distanceMax - distanceMin) * rand() / RAND_MAX + distanceMin;
        
        // Set the distance of the edge
        setEdgeValue(vertexX, vertexY, distance);
      }
    }
  }
}

// Function overloading the operator <<
ostream& operator<<(ostream& out, cGraph& graph){
  out << "Graph:" << endl;
  for (auto it = graph.vertices.begin(); it != graph.vertices.end(); ++it){
    cVertex* vertex = *it;
    out << *vertex;
  }
  out << endl;
  return out;
}

