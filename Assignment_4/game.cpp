// Objective: make a playable version of the game of HEX

// Source:
//  * http://www.cplusplus.com

// Explanation:
//  I decided to represent the board of this game by a graph. More concretly, I defined my graph (cf. cGraph module) as a vector 
//  of vertex pointers. Each vertex (cf. cVertex module) is represented by an id, a color, a value (priority in the Dijkstra context) 
//  and a list of edge pointers. Each edge (cf. cEdge module) is defined by its cost and a remote vertex id. Then, I created 4 virtual 
//  vertexes which represent the different borders (NORTH, SOUTH, WEST and EAST). Finally to identify if a player (BLUE or RED) wins 
//  the party, I reused Prim's algorithm from one of the corresponding virtual vertex in order to evaluate if the opposite one belongs 
//  to the generated MST.

// Dependancies
#include <iostream>
#include <stdexcept>
#include "cHex.hpp"

// Namespace
using namespace std;

// Constant representing the number of hexes possible
const size_t HEX_BOARD_7X7 = 7;
const size_t HEX_BOARD_11X11 = 11;

// Main function
int main(void){
	
	// Print a description
	cout << "Welcome to my version of the Hex game" << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	
	// Read the number of hexes
	size_t numberHexes (0);
	bool retry (false);
	do {
		
		// Request the number of hexes
		cout << "Please, enter a number of hexes on 1 side (7 or 11): ";
		string answer;
		cin >> answer;
		try {
			numberHexes = stoi(answer);
			retry = (numberHexes != HEX_BOARD_7X7) and (numberHexes != HEX_BOARD_11X11);
			if (retry) {
				cout << "ERROR - Invalid number of hexes!" << endl;
			}
		} catch (const invalid_argument& arg){
			cout << "ERROR - Invalid number of hexes!" << endl;
			retry = true;
		}
		
	} while (retry);
	cout << "--------------------------------------------------------------------------------" << endl;
	
	// Initialize the game
	cHex game = cHex(numberHexes);
	game.play();
	
  // Return OK
  return 0;
}

