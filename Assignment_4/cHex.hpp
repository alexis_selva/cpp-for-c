// Module for representing the hex game

#ifndef CHEX_HPP
#define CHEX_HPP

// Dependancies
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <iterator>
#include "cGraph.hpp"
#include "cMst.hpp"

// Namespace
using namespace std;

// Constant representing the virtual vertices
const size_t NB_VIRTUAL_VERTICES = 4;

// Class: cHex
class cHex {
	
	private:
		size_t dimension;
		size_t moves;
		cGraph board;

	public:
		cHex(size_t dimension);
		~cHex(void);
		size_t getDimension(void);
		size_t getMoves(void);
		cGraph& getBoard(void);
		void play(void);
		
	private:
		void initializeVirtualVertex(size_t begin, size_t sentinel, size_t increment, eVertexColor color, size_t virtualVertex);
		void initialize(void);
		void next(eVertexColor color);
		bool win(eVertexColor color);
		void printRules(void);
		void printBoard(void);
		
		// Function determining if the vertex is located on the upper bound of the hex board
		inline bool isUpBound(size_t vertex){
			return (vertex / dimension == 0);
		};

		// Function determining if the vertex is located on the lower bound of the hex board
		inline bool isLowBound(size_t vertex){
			return (vertex / dimension == dimension - 1);
		};

		// Function determining if the vertex is located on the left bound of the hex board
		inline bool isLeftBound(size_t vertex){
			return (vertex % dimension == 0);
		};

		// Function determining if the vertex is located on the right bound of the hex board
		inline bool isRightBound(size_t vertex){
			return (vertex % dimension == dimension - 1);
		};

		// Function determining if the vertex is out of bound of the hex board
		inline bool isOutOfBound(size_t vertex){
			return (vertex >= dimension * dimension);
		};
		
		// Function determining if the vertex is virtual
		inline bool isVirtual(size_t vertex){
			return (vertex >= dimension * dimension) and (vertex <= dimension * dimension + 3);
		};
		
		// Function getting the NORTH virtual vertex
		inline size_t getNorthVirtualVertex(void) {
			return dimension * dimension;
		};
				
		// Function getting the SOUTH virtual vertex
		inline size_t getSouthVirtualVertex(void) {
			return dimension * dimension + 1;
		};
		
		// Function getting the WEST virtual vertex
		inline size_t getWestVirtualVertex(void) {
			return dimension * dimension + 2;
		};
		
		// Function getting the EAST virtual vertex
		inline size_t getEastVirtualVertex(void) {
			return dimension * dimension + 3;
		};
};

#endif
