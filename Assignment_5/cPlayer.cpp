// Module for representing a player

// Dependancies
#include "cPlayer.hpp"

// Constructor: generate the player
cPlayer::cPlayer(const ePlayerColor& color, const ePlayerMode& mode) : color(color), mode(mode) {
}

// Destructor: destroy the player
cPlayer::~cPlayer(void) {
}

// Accessor: get the player color
ePlayerColor cPlayer::getColor(void) const {
	return color;
}

// Accessor: get the player mode
ePlayerMode cPlayer::getMode(void) const {
	return mode;
}

// Mutator: set the player color
void cPlayer::setColor(const ePlayerColor& color) {
	this->color = color;
}

// Mutator: set the player mode
void cPlayer::setMode(const ePlayerMode& mode) {
	this->mode = mode;
}

// Function overloading the operator <<
ostream& operator<<(ostream& out, cPlayer& player){
  
  // Determine the player mode ?
  if (player.mode == ePlayerMode::AI){
		out << "AI (";
	} else {
		out << "HUMAN (";
	}
	out << toString(player.color) << ")";
  return out;
}
