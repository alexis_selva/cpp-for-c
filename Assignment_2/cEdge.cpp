// Module for representing an edge

#ifndef CEDGE_CPP
#define CEDGE_CPP

// Dependancies
#include <iostream>

// Namespace
using namespace std;

// Constante to limit the number of decimals
const int DECIMALS = 3;

// Class: cEdge
class cEdge{
  
  private:
    double cost;
    size_t vertex;
  
  public:
    cEdge(size_t vertexId);
    ~cEdge(void);
    double getCost(void);
    size_t getVertex(void);
    void setCost(double newCost);
    void setVertex(size_t newVertexId);
};

// Constructor: create a new edge
cEdge::cEdge(size_t vertexId){
  cost = 0;
  vertex = vertexId;
}

// Destructor: destroy a edge
cEdge::~cEdge(void){
}

// Accessor: get the cost
inline double cEdge::getCost(void){
  return cost;
}

// Accessor: get the remote vertex
inline size_t cEdge::getVertex(void){
  return vertex;
}

// Mutator: modify the cost
inline void cEdge::setCost(double newCost){
  cost = newCost;
}

// Mutator: modify the remote vertex
inline void cEdge::setVertex(size_t newVertexId){
  vertex = newVertexId;
}

// Function overloading the operator ==
inline bool operator== (cEdge& edge1, cEdge& edge2){
  return (edge1.getVertex() == edge2.getVertex());
}

// Function overloading the operator ==
inline bool operator== (cEdge& edge, size_t id){
  return (edge.getVertex() == id);
}
 
// Function overloading the operator !=
inline bool operator!= (cEdge& edge1, cEdge& edge2){
  return !(edge1 == edge2);
}
 
// Function overloading the operator !=
inline bool operator!= (cEdge& edge, size_t id){
  return !(edge == id);
}

// Function overloading the operator <<
inline ostream& operator<<(ostream& out, cEdge& edge){
  out.precision(DECIMALS);
  out << "\t----\t" << edge.getCost() << "\t----\t" << edge.getVertex() << endl;
  return out;
}

#endif
