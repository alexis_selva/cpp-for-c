// Module for representing an edge

#ifndef CEDGE_HPP
#define CEDGE_HPP

// Dependancy
#include <iostream>

// Namespace
using namespace std;

// Constante to limit the number of decimals
const int DECIMALS = 3;

// Class: cEdge
class cEdge{
  
  private:
    double cost;
    size_t vertex;
  
  public:
    cEdge(size_t vertexId);
    cEdge(const cEdge& edge);
    ~cEdge(void);
    double getCost(void);
    size_t getVertex(void);
    void setCost(double newCost);
    void setVertex(size_t newVertexId);
    friend bool operator== (cEdge& edge1, cEdge& edge2);
		friend bool operator== (cEdge& edge, size_t id);
		friend bool operator!= (cEdge& edge1, cEdge& edge2);
		friend bool operator!= (cEdge& edge, size_t id);
		friend ostream& operator<<(ostream& out, cEdge& edge);
};

#endif
