// Module for representing a vertex

// Dependancy
#include "cVertex.hpp"

// Constructor: : create a new vertex
cVertex::cVertex(size_t vertexId) : id(vertexId), color(eVertexColor::WHITE), value(0) {
}

// Constructor: : create a new vertex from an existing one
cVertex::cVertex(const cVertex& vertex) : id(vertex.id), color(vertex.color), value(vertex.value) {
		
	// Copy all edges
	for (auto edge : vertex.edges){
		
		// Construct a new vertex from the current one
		cEdge * newEdge = new cEdge(*edge);
		
		// Add this new edge to the list of edges of the new vertex
		this->edges.push_back(newEdge);
	}
}

// Destructor: : destroy the vertex
cVertex::~cVertex(void){
}

// Accessor: get the id of the vertex
size_t cVertex::getId(void){
  return id;
}

// Accessor: get the color of the vertex
eVertexColor cVertex::getColor(void){
  return color;
}

// Accessor: get the value of the vertex
double cVertex::getValue(void){
  return value;
}

// Accessor: get the edges (reference)
list <cEdge*>& cVertex::getEdges(void){
  return edges;
}

// Mutator: set the id of the vertex
void cVertex::setId(size_t newVertexId){
  id = newVertexId;
}

// Mutator: set the color of the vertex
void cVertex::setColor(const eVertexColor& color){
	this->color = color;
}

// Mutator: set the value of the vertex
void cVertex::setValue(double newValue){
  value = newValue;
}

// Mutator: set the edges
void cVertex::setEdges(const list <cEdge*>& newEdges){
  edges = newEdges;
}

// Function adding a edge between 2 vertices
void cVertex::add(size_t vertexId){
  if (!isPresent(vertexId)){
    edges.push_back(new cEdge(vertexId));
  }
}

// Function checking if an edge exists between 2 vertices
bool cVertex::isPresent(size_t vertexId){
  cEdge* edge(nullptr);  
  bool found(false);
  auto it = edges.begin();
  bool loop = it!=edges.end();
  while (loop){
      
    // Get the edge (reference)   
    edge = *it;
      
    // Is it the one we are looking for ?  
    found = (*edge == vertexId);
    
    // Get the next ?
    ++it;
    
    // Shall we continue ?
    loop = (it!=edges.end()) && (!found);
  }
  
  return found;
}

// Function getting the cost of a edge corresponding to a particular remote vertex
double cVertex::getCost(size_t vertexId){
  cEdge* edge(nullptr);   
  double cost(0.0);
  bool found(false);
  auto it = edges.begin();
  bool loop = it!=edges.end();
  while (loop){
      
    // Get the edge (reference)   
    edge = *it;
    
    // Is it the one we are looking for ?
    found = (*edge == vertexId);
    
    // Get the next
    ++it;
    
    // Shall we continue ?
    loop = (it!=edges.end()) && (!found);
  }
  
  // Get the cost
  if (found){
    cost = edge->getCost();
  } 
  
  return cost;
}

// Function setting the cost of an edge corresponding to a particular remote vertex
void cVertex::setCost(size_t vertexId, double newCost){
  cEdge* edge(nullptr); 
  bool found(false);
  auto it = edges.begin();
  bool loop = it!=edges.end();
  while (loop){
      
    // Get the edge (reference) 
    edge = *it;
    
    // Is it the one we are looking for ?
    found = (*edge == vertexId);
    
    // Get the next
    ++it;
    
    // Shall we continue ?
    loop = (it!=edges.end()) && (!found);
  }
  
  // Set the cost
  if (found){
    edge->setCost(newCost);
  }
}

// Function removing an existing edge between 2 vertices
void cVertex::remove(size_t vertexId){
  cEdge* edge(nullptr); 
  bool found(false);
  auto it = edges.begin();
  bool loop = it!=edges.end();
  while (loop){
      
    // Get the edge (reference) 
    edge = *it;
    
    // Is it the one we are looking for ?
    found = (*edge == vertexId);
    
    // Get the next
    ++it;
    
    // Shall we continue ?
    loop = (it!=edges.end()) && (!found);
  }
  
  // Remove the edge
  if (found){
    edges.remove(edge);
    delete(edge);
  }
}

// Function clearing any existing edges between 2 vertices
void cVertex::clear(void){
  if (!edges.empty()){
    edges.clear();  
  }
}

// Function overloading the operator ==
bool operator== (cVertex& vertex1, cVertex& vertex2){
  return (vertex1.id == vertex2.id);
}

// Function overloading the operator ==
bool operator== (cVertex& vertex, size_t vertexId){
  return (vertex.id == vertexId);
}
 
// Function overloading the operator !=
bool operator!= (cVertex& vertex1, cVertex& vertex2){
  return !(vertex1 == vertex2);
}

// Function overloading the operator <<
ostream& operator<<(ostream& out, cVertex& vertex){
  if (!vertex.edges.empty()){
    for (auto it = vertex.edges.begin(); it != vertex.edges.end(); ++it){
      cEdge* edge = *it;
      out << toString(vertex.color) << " " << vertex.id << *edge;
    }
  } else {
		out << toString(vertex.color) << " " << vertex.id;
	}
  return out;
}

